#!/bin/bash

if [[ -n "${bamboo_buildKey+set}" ]]; then
    # In Bamboo, check if the dockerfile has changed...
    tmp=`mktemp`
    git diff --name-only "${bamboo_repository_previous_revision_number}" > $tmp
    grep 'docker/dac-build.dockerfile' $tmp > /dev/null
    if [ $? -eq 1 ]; then
        echo "+++ Docker build file has not changed, skipping build..."
        exit 0
    fi
fi

set -e

echo "+++ Docker build file has changed, building..."

REPO=docker.atlassian.io/atlassian/dac-build

docker build --tag ${REPO} -f docker/dac-build.dockerfile .

docker push ${REPO}

