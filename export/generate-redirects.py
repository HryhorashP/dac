#!/usr/bin/env python

import argparse
import glob
import logging as log
import os.path
import sys
import re
from pprint import pprint

import yaml

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

def get_options():
    parser = argparse.ArgumentParser(description='Generate redirects from migrator.yaml')
    parser.add_argument('-c --config-file', dest="config_file",
                        metavar='config file', default="migrator.yaml",
                        help='Name of the config file used to specify the target structure')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='verbose: print out debug information')
    parser.add_argument('-e', '--excel', action='store_true',
                        help='generate CSV file')
    options = parser.parse_args()
    log.basicConfig(level=log.DEBUG if options.verbose else log.INFO, format=LOG_FORMAT)
    return options

def main():
    opt = get_options()
    stream = open(opt.config_file, "r")
    config = yaml.load(stream)
    if opt.excel:
        for e in config['migrations'][0]['files']:
          f = os.path.splitext(e['file'])[0] + ".html"
          m = os.path.splitext(e['move'])[0] + "/"
          print ("/" + f + ", /" + m)
    else:
        for e in config['migrations'][0]['files']:
          f = os.path.splitext(e['file'])[0] + ".html"
          m = os.path.splitext(e['move'])[0] + "/"
          print ("RewriteRule ^/" + f + "(.*)$ https://%{HTTP:host}/" + m + " [R=301,L]")
    return 0


if __name__ == '__main__':
    sys.exit(main())
