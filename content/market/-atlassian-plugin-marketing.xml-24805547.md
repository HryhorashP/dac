---
title: Atlassian Plugin Marketing.Xml 24805547
aliases:
    - /market/-atlassian-plugin-marketing.xml-24805547.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24805547
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24805547
confluence_id: 24805547
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_atlassian-plugin-marketing.xml

``` javascript
<atlassian-plugin-marketing>
       
      <!--Describe names and versions of compatible applications -->
      <compatibility>
        <product name="jira" min="4.0" max="4.4.1"/>
        <product name="bamboo" min="3.0" max="3.1"/>
      </compatibility>
       
      <!-- Describe your add-on logo and banner. The banner is only displayed in the UPM. -->
      <logo image="images/01_logo.jpg"/>
      <banner image="images/02_banner.jpg"/>
       
      <!-- Describe highlight and cropped highlight image assets, in order. -->
      <highlights>
        <highlight image="images/03_highlight1_ss.jpg" cropped="images/04_highlight1_cropped.jpg"/>
        <highlight image="images/05_highlight2_ss.jpg" cropped="images/06_highlight2_cropped.jpg"/>
        <highlight image="images/07_highlight3_ss.jpg" cropped="images/08_highlight3_cropped.jpg"/>
      </highlights>
       
      <!-- Describe additional screenshots you'd like listed on your add-on listing. -->
      <screenshots>
        <screenshot image="images/09_addl_ss1.jpg"/>
        <screenshot image="images/10_addl_ss2.jpg"/>
        <screenshot image="images/11_addl_ss3.jpg"/>
      </screenshots>
  
</atlassian-plugin-marketing>
```



