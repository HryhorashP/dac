---
title: Resize Warning 10421020
aliases:
    - /market/-resize-warning-10421020.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10421020
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10421020
confluence_id: 10421020
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Resize warning

**Some important information about these guidelines**

These guidelines specify sizes for various branding images. The maximum height and width in these guidelines must be met. We will size down images that exceed these maximum values. For example, if you upload a logo that is 108x108px. We resize it down to 72x72px. If you upload a logo that is 108x52px we adjust it until the width is 72px.

If you upload a logo that is too small, for example 32x32px, we will not size it up. Instead, we'll center it and fill the background. If you upload a logo with a single dimension that is too small, such as 72x54px, we will center it and fill the background to make it match.

If you have a legacy image that does not meet a guideline, we will resize it as specified above. In no case are animated GIFs permitted; we will ask you to resubmit.



