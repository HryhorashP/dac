---
title: Converting Proprietary Licenses Into Marketplace Licenses 10421889
aliases:
    - /market/converting-proprietary-licenses-into-marketplace-licenses-10421889.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10421889
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10421889
confluence_id: 10421889
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Converting proprietary licenses into Marketplace licenses

-   [Before starting]
-   [Using the License Service API]
-   [Request format]
-   [Successful response]
-   [Error responses]

If your add-on used a non-Atlassian licensing system before becoming a paid-via-Atlassian add-on, you can convert your proprietary license keys into new Marketplace license keys. To convert an existing license into a Marketplace license, you use the Atlassian License Service API, as described here.

## Before starting

Before you can use the Atlassian License Service API, you need to be approved for access by the Atlassian Marketplace team. This is necessary because the Atlassian Marketplace team audits usage and monitors the performance of the API very closely, subject to the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Atlassian Marketplace Vendor Agreement</a>. 

To be approved for access, file an issue with a security level of **Reporters and Atlassian Staff** in the Marketplace JIRA instance: 

<a href="https://ecosystem.atlassian.net/browse/AMKT" class="uri" class="external-link">https://ecosystem.atlassian.net/browse/AMKT</a>

Keep an eye on the issue (or watch for notifications) to find out when you are ready to go.

## Using the License Service API

After getting the go-ahead from the Marketplace team, use the Atlassian License Service API as described here. The service accepts JSON data via HTTP POST and returns a JSON response.

The URL for the service is:

    https://marketplace.atlassian.com/rest/1.0/plugins/KEY/license

In your request:

-   Replace `KEY` with the plugin key of your add-on. This is the same key you supplied when submitting your add-on to the Marketplace. It also appears in the plugin descriptor (`atlassian-plugin.xml`) for your add-on.
-   The `Content-Type` must be `application/json`.
-   You must provide credentials via HTTP basic authentication. You can use the same username and password that you do to access your vendor account on the <a href="https://marketplace.atlassian.com/" class="external-link">Atlassian Marketplace</a>.

For example, in the following transaction, we're migrating a 2000 user legacy license:

    POST https://marketplace.atlassian.com/rest/1.0/plugins/my.plugin.com/license
    Content-Type: application/json
    {
      "id": "1000",
      "email": "customer@example.com",
      "firstName": "Jane",
      "lastName": "Smith",
      "organisationName": "Example Customer",
      "isoCountryCode": "US",
      "startDate": "2011-05-01",
      "endDate": "2012-05-01",
      "licenseType": "COMMERCIAL",
      "users": 2000
    }

     -- response --
    200 OK
    Content-Type:  application/json;charset=UTF-8
    Content-Length:  518
    Server:  Jetty(8.0.4.v20111024)
    {"id":"1000",
      "sen":"SEN-4839484",
      "licenseKey":"888BKQ0OD8oPeNpVj11Lwz8U08GytI1qJ8Z2I=X02eu"}

Notice the following:

-   The request data includes a unique license `id`. If the request data provides the `id` of an existing Atlassian license, the service returns an `HTTP 409 (Conflict)` and the response body contains the same data returned by a successful call.
-   The service responds with a 400 (Bad Request) if the `startDate` of the imported license is less than 30 days ago. The license must be at least 30 days old before converting it into a Marketplace license.

As another example, here's the request for an unlimited user license:

    POST https://marketplace.atlassian.com/rest/1.0/plugins/my.plugin.com/license
    Content-Type: application/json
    {
      "id": "1000",
      "email": "customer@example.com",
      "firstName": "Jane",
      "lastName": "Smith",
      "organisationName": "Example Customer",
      "isoCountryCode": "US",
      "startDate": "2011-05-01",
      "endDate": "2012-05-01",
      "licenseType": "COMMERCIAL",
      "users": -1
    }

Notice that a `users` value of -1 indicates an unlimited number of users.

## Request format

The service accepts the following values in the request. 

Make sure that the data you submit in your request is formatted as indicated in the table. For example, dates must be in `yyyy-MM-dd` format.  Submitting incorrectly formatted data does not necessarily generate a service error and can lead to faulty or invalid licenses.

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Field</p></th>
<th><p>Required?</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>id</code></p></td>
<td><p>yes</p></td>
<td><p>A unique license identifier. The identifier prevents accidentally importing the same license twice. This can be any non-empty string; it only must be unique within the set of all licenses for this particular add-on.</p></td>
</tr>
<tr class="even">
<td><p><code>email</code></p></td>
<td><p>yes</p></td>
<td><p>The licensee's (end customer's) email address. If there is not already an account on <a href="http://my.atlassian.com/" class="external-link">my.atlassian.com</a> with this email, the service creates one.</p></td>
</tr>
<tr class="odd">
<td><p><code>firstName</code>, <code>lastName</code>, <code>organisationName</code></p></td>
<td><p>yes</p></td>
<td><p>The licensee's (end customer's) identifying information. Used to create the new account on <a href="http://my.atlassian.com/" class="external-link">my.atlassian.com</a>. This is only done if the account did not previously exist. If the account exists, the service ignores these fields.</p></td>
</tr>
<tr class="even">
<td><p><code>address1</code>, <code>address2</code>, <code>city</code>, <code>state</code>, <code>postcode</code></p></td>
<td><p>no</p></td>
<td><p>Address information. Used for licensees (end customers) without an account on <a href="http://my.atlassian.com/" class="external-link">my.atlassian.com</a>. The service creates the new account with this data. Otherwise, if the account exists, the system ignores this information.</p></td>
</tr>
<tr class="odd">
<td><p><code>isoCountryCode</code></p></td>
<td><p>yes</p></td>
<td><p>The ISO code for the licensee's (end customer's) country. The service can only import a licenses for Marketplace-authorised countries.</p></td>
</tr>
<tr class="even">
<td><p><code>startDate</code></p></td>
<td><p>yes</p></td>
<td><p>The date on which support for this license started. This value should have a format of: <code>yyyy-MM-dd</code></p></td>
</tr>
<tr class="odd">
<td><p><code>endDate</code></p></td>
<td><p>yes</p></td>
<td><p>The date on which support for this license ends. This value should have a format of: <code>yyyy-MM-dd</code></p></td>
</tr>
<tr class="even">
<td><p><code>licenseType</code></p></td>
<td><p>yes</p></td>
<td><p>Must be <code>COMMERCIAL</code>, <code>ACADEMIC</code>, or <code>STARTER</code>. Use <code>STARTER</code> for add-on licenses at the host application's Starter level: 10 users (JIRA or Confluence), 5 users (Crucible), or no remote agents (Bamboo).</p></td>
</tr>
<tr class="odd">
<td><p><code>users</code></p></td>
<td><p>yes</p></td>
<td><p>The maximum number of users (or, for Bamboo, the maximum number of remote agents) supported by this license, or <code>-1</code> for no maximum. This field is numeric, not a string.</p></td>
</tr>
<tr class="even">
<td><p><code>expertEmail</code></p></td>
<td><p>no</p></td>
<td><p>If set, the imported license will be associated with the Atlassian Expert organization associated to this email address.</p></td>
</tr>
</tbody>
</table>

**Notes:**

1.  The times you specify will be set to midnight Sydney time on that day. This might be as much as a day ahead relative to the location of your customer.
2.  In light of the discontinuation of the <a href="https://www.atlassian.com/licensing/enterprise-faq#legacyenterpriseofferings-1" class="external-link">legacy Enterprise licensing program</a>, this service generates add-on licenses that are compatible with both enterprise and non-enterprise host licenses.

## Successful response

An HTTP 200 status indicates the license was converted successfully. The JSON response body contains the following:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><code>id</code></p></td>
<td><p>The same unique identifier specified in the request.</p></td>
</tr>
<tr class="even">
<td><p><code>sen</code></p></td>
<td><p>The new Support Entitlement Number for this license.</p></td>
</tr>
<tr class="odd">
<td><p><code>licenseKey</code></p></td>
<td><p>The encoded Marketplace-license key.</p></td>
</tr>
</tbody>
</table>

## Error responses

If the call fails, the service returns one of the following HTTP statuses:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><code>400 (Bad Request)</code></p></td>
<td><p>Reasons for this error include: the request did not contain a valid JSON object, the <code>startDate</code> of the imported license is within the last 30 days, the <code>expertEmail</code> is not a valid Atlassian Expert email address, or a required field was omitted.</p>
<p>If available, the response returns additional detail about the error.</p></td>
</tr>
<tr class="even">
<td><p><code>401 (Unauthorized)</code></p></td>
<td><p>You did not provide valid basic authentication credentials.</p></td>
</tr>
<tr class="odd">
<td><p><code>403 (Forbidden)</code></p></td>
<td><p>The credentials you provided were not associated with the vendor of the specified add-on or the vendor is not authorized to use the API.</p></td>
</tr>
<tr class="even">
<td><p><code>404 (Not Found)</code></p></td>
<td><p>The plugin key you specified in the URL does not correspond to any add-on listed on the Atlassian Marketplace.</p></td>
</tr>
<tr class="odd">
<td><p><code>409 (Conflict)</code></p></td>
<td><p>A license with the same <code>id</code> already exists. The response body contains the same JSON information as it would for a successful import. This allows you to view the license key and SEN for an existing license.</p></td>
</tr>
</tbody>
</table>

 

 

 

  [Before starting]: #before-starting
  [Using the License Service API]: #using-the-license-service-api
  [Request format]: #request-format
  [Successful response]: #successful-response
  [Error responses]: #error-responses

