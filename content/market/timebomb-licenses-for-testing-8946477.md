---
title: Timebomb Licenses for Testing 8946477
aliases:
    - /market/timebomb-licenses-for-testing-8946477.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8946477
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8946477
confluence_id: 8946477
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Timebomb Licenses for Testing

While using UPM's licensing API you will likely find yourself wanting to test out your plugin with a valid license entered.

## General Testing

The following license will work for any marketplace-enabled plugin. Click an **expand source** line for any of the licenses below to reveal the key. Then, copy and paste into a license key field:

**3 Hour Expiration**

``` javascript
AAABCA0ODAoPeNpdj01PwkAURffzKyZxZ1IyUzARkllQ24gRaQMtGnaP8VEmtjPNfFT59yJVFyzfu
bkn796Ux0Bz6SmbUM5nbDzj97RISxozHpMUnbSq88poUaLztFEStUN6MJZ2TaiVpu/YY2M6tI6sQ
rtHmx8qd74EZ+TBIvyUU/AoYs7jiE0jzknWQxMuifA2IBlUbnQ7AulVjwN9AaU9atASs69O2dNFU
4wXJLc1aOUGw9w34JwCTTZoe7RPqUgep2X0Vm0n0fNut4gSxl/Jcnj9nFb6Q5tP/Ueu3L+0PHW4g
hZFmm2zZV5k6/95CbR7Y9bYGo/zGrV3Ir4jRbDyCA6vt34DO8p3SDAsAhQnJjLD5k9Fr3uaIzkXK
f83o5vDdQIUe4XequNCC3D+9ht9ZYhNZFKmnhc=X02dh
```

 

 

Additionally, the following licenses allow you to test what happens when a valid license becomes expired:

**60 Second Expiration**

``` javascript
AAABEA0ODAoPeNp9UE1Pg0AUvO+v2MSbCc0uQZOS7KEIUWMtpNJqGi9bfKUb4S3ZD7T/XgrqwYPv9
mbezGTeRXn0NK8cZRHlPGZRHEW0SEsaMh6SFGxlVOeURlGCdbRRFaAFetCGdo2vFdI36KHRHRhLV
r7dg8kPGztsgjNyY0Cexal0IELOw4DNA85J1svGj4xwxgOZrOzsciYrp3qY0Eep0AFKrCD77JQ5j
TapN6PyNb5mw5Dc1BKVndwWrpHWKonkCUwP5j4Vye28DF422yh42O3ugoTxZ7KcagzsBt9Rf+AP8
k/O90V56mAl24HPttkyL7L1b+1Etnut19BqB4sa0FkRXpHCm+ooLfz9wRfgrX9WMCwCFAkWHvhJC
dutS3LcZ46iYgICDPQqAhQL76vdT4AYTQXBwl/wbw/MtQrP4w==X02dt
```

**10 Second Expiration**

``` javascript
AAABEA0ODAoPeNp9UE1Pg0AUvO+v2MSbCc0uoYeScChC1FhLU6Gaxsvr+ko3wi7ZD7T/XoTqwYPv9
mbezGTeVXnytBCOsohyHrN5HHG6yUoaMh6SDK0wsnNSq6RE62gjBSqL9KgN7RpfS0XfsMdGd2gsW
fv2gKY4VnbYEs7IjUH4FmfgMAk5DwO2CDgneQ+NH5nEGY9ksrKz6xkIJ3uc0EeQyqECJTD/7KQ5j
zaZN6PyNeZsGFKYGpS0k9vSNWCtBEWe0PRo7rMkvV2UwUu1i4KH/f4uSBl/JqupxsBW6l3pD/WD/
JNzuSjPHa6hHfh8l6+KTb79rZ1Ce9B6i612uKxROZuEc7LxRpzA4t8ffAHYfn9KMCwCFEErfsC77
7XOAsdjoKVRM24pJL3+AhRbORJTyFn7+5BUotohYeGfCqYgkA==X02dt
```

If you're running JIRA Software, JIRA Core or JIRA Service Desk and need to license any of them, you can do so by using the following license key on the **Versions & licenses** page:

**3 Hour Expiration for all Atlassian host applications**

``` javascript
AAACLg0ODAoPeNqNVEtv4jAQvudXRNpbpUSEx6FIOQBxW3ZZiCB0V1WllXEG8DbYke3A8u/XdUgVQ
yg9ZvLN+HuM/e1BUHdGlNvuuEHQ73X73Y4bR4nbbgU9ZwFiD2IchcPH+8T7vXzuej9eXp68YSv45
UwoASYhOeYwxTsIE7RIxtNHhwh+SP3a33D0XnntuxHsIeM5CIdwtvYxUXQPoRIF6KaC0FUGVlEB3
v0hOAOWYiH9abFbgZith3i34nwOO65gsAGmZBhUbNC/nIpjhBWEcefJWelzqIDPWz/OtjmXRYv2X
yqwnwueFkT57x8e4cLmbCD1QnX0UoKQoRc4EUgiaK4oZ2ECUrlZeay75sLNs2JDmZtWR8oPCfWZG
wHAtjzXgIo0SqmZiKYJmsfz8QI5aI+zApuq6fqJKVPAMCPnNpk4LPW6kBWgkZb+kQAzzzS2g6Dnt
e69Tqvsr4SOskIqEFOeggz1v4zrHbr0yLJR8rU64FpQpVtBy1mZxM4CnHC9Faf8tKMnTF1AiXORF
ixyQaWto3RZ+ncWLXtMg6EnKZZRpmQNb2R8tnJXFulCfXmXLry7TrHBWn2HNVyH8WYxj9AzmsxiN
L/R88Xg6rA1lVs4QpO5titxhplJcCY2mFFZLutAZVhKipm15/VhJx36YVqyN8YP7IaGC1+lwnJ7Q
5pJpNmxk5hP3qovutY8Pi4E2WIJ59esnr1p+T6eD67teBVCHf+ga+ho4/4D9YItZDAsAhQ5qQ6pA
SJ+SA7YG9zthbLxRoBBEwIURQr5Zy1B8PonepyLz3UhL7kMVEs=X02q6
```

 

## Testing for License Error Conditions

If you are testing with the SDK, your Atlassian host application (JIRA, Confluence, etc) generates with a 5 user test license for plugin developers. You can install a plugin license that supports more users than this; you cannot install one that supports fewer. This is important, as many of the [errors returned by the Plugin Licensing API] result from mismatches between the host application and the plugin license.

To test a nearly expired evaluation license:

**10 user evaluation license, expires in three hours**

``` javascript
AAAA9A0ODAoPeNpdj0FPwkAQhe/7KzbxZlLSXTEGkj2AbYBoSxO2SriNdcCNZdrMbqv8e5HqheP7X
ubLvJusIZkBSzWRSk3VwzQeyyKxUsdKiwR9xa4NriFj0QdZuwrJo9w3LNu6OziS79hj3bTIXuTd8
Q15vS/9ORkVi0dG+D1OIKDRSuko1pG+F2kPdXdpTOAOxaDyo9sRVMH1ONAMHAUkoArT79bx6aIp7
pZizQcg5wfDLNTgvQMSG+QeeZWY+WJio235Mo6edrtlNI/Vq3geXj+3JX1S80X/5Mr9R+2pxRyOa
Gy6sat8IYqOqw/weL3lB7zIa8swLAIULU/BlyZoOVbLPEVGAVAio9Yh3t4CFFBJJd00fLnkkzgex
V+T6NsiPVpgX02ck
```

To test an expired evaluation license:

**10 user evaluation license, already expired**

``` javascript
AAAA9A0ODAoPeNpdj0FrwkAQhe/7KxZ6K0SySyso7KGaoNImBty0xduYjnZpnITZTVr/fa2xF4/ve
8zHvLusIZkBSzWRSk3VeKrHskis1LHSIkFfsWuDa8hY9EHWrkLyKPcNy7buDo7kB/ZYNy2yF3l33
CGv96U/J6NiMWeEv+MEAhqtlI5iHelHkfZQd5fGBO5QDCo/uh9BFVyPA83AUUACqjD9aR2fLpoiX
oo1H4CcHwxPoQbvHZDYIPfIq8TMFhMbvZevD9HzdruMZrF6Ey/D6+e2pC9qvumf3Liv1J5azOGIx
qYbu8oXoui4+gSPt1t+Ab4Oa8gwLAIUbz98vdLFn/2d+epiSRUkE1spDVICFGIao7aIxBdxQdEoM
o/wg3GIMhntX02ck
```

To test a nearly expired license:

**10 user license, expires in three hours**

``` javascript
AAABCQ0ODAoPeNpdj8FPgzAYxe/9K5p4M2FpcYtuSQ9DyLaoQFxRs9s3/GCN0JK2oPvvnUM97Pjey
/vlvSt56GlWesqmlLPF7HbB7mgeSxoyHpIYXWlV55XRQqLztFElaoe0MpZ2TV8rTd9xwMZ0aB1J+
3aPNqsKd1KCM3JvEX7KMXgUIedhwOYB5yQZoOnPiaigcUhGlptcT6D0akDhbY/kCZT2qEGXmHx1y
h7PnPxmTTJbg1ZuRCx9A84p0GSLdkC7iUW0msvgrXiZBg+73TqIGH8lj+P2U1roD20+9Z9zwf515
bHDFFoUMtnKTbr6fxdBuzfmGVvjcVmj9k6EM5L3tjyAw8ur38lTdxIwLAIUOawOiPVfH6zNRA/o/
SZKylVzOfgCFG0JSQn0He9aMn3zrNE/fg625LgFX02dh
```

To test an already expired license:

**12 user license, already expired**

``` javascript
AAABBw0ODAoPeNpdjzFvgzAUhHf/CkvdKhHZkA6J5CEUlERtCSqmrbK90Ae1CjayDW3+fdMglox3p
/t0dycHpJkZKQ8pW62X0ZozmheShoxHJEFXWdV7ZbSQ6DxtVYXaIa2NpX07NErTTxyxNT1aR7KhO
6E91KW7KMFD8mgR/ssJeBQh51HAWRAyko7QDtdE1NA6JBPLLe4XUHk1ovB2QPICSnvUoCtMf3tlz
1dOznbkYBvQyk2IjW/BOQWaFGhHtPtExNuVDD7Kt2XwdDzugpjxd/I8bb+kpf7W5kfPzg17fhFDd
zLmFTvjcdOg9k6ED3NHnnvMoEMh00Lusy3JB1t9gcPbq3/qq3cVMCwCFEJl98Z/glYXAJfalw64u
yFp+3zWAhRYkVu511dtvnuBWgWES2DhAbIkpg==X02dh
```

To test a user-mismatch error (non-Bamboo host application) or edition-mismatch error (Bamboo host application):

**2 user / 1 remote-agent license, expires in three hours**

``` javascript
AAABCA0ODAoPeNpdj8FrwjAYxe/5KwK7DSpJK4hCDnYtKttame0c3j7j1y6sTUqSdvO/n7PbDh7fe
7wf793t8URz6SmLKJ8totmCTek2KWjIeEgSdNKqziujRYHO00ZJ1A5pZSztmr5Wmp5wwMZ0aB3J+
vaINq9Kd1EiJA8W4aebgEcRch4GbB5wRtIBmv6aiAoah2REucn9BKRXAwpveyTPoLRHDVpi+tUpe
75yttGa5LYGrdyIWPoGnFOgyQ7tgHaTiHg1L4K38nUaPB4O6yBmfE+exumXtNQf2nzqP+eG/esW5
w4zaFEU6a7YZKv/czG0R2NesDUelzVq7wQn297Kd3B4+/Qb7dx2mjAtAhUAkCqinKy6DbTosPd7S
mDta0BxH8ECFBU82orr2+LLHzvHmq2Xr3pvQc8mX02dh
```

To test a version-mismatch error:

**10 user license, maintenance expired before your plugin was built**

``` javascript
AAABDQ0ODAoPeNpdj01Lw0AQhu/7Kxa8CVt204KmsIfG1rZoP7CJSm/TOK2LyW6Y3UT7740JFRHmM
u/LPMNzdU+Gr4C4vOUyHo/UeBTz7S7lkVRDNkWfk6mCcVan6AMvTI7WIz864lVRn4zlb9hg4Sokz
9Z1eUDaHDPfblpJdkcIP8dTCKgjpYZCRqLlzhoo6q7RgWpkPcoPrgeQB9Ngn67A2IAWbI6zr8rQu
cOo+EYKqdphGzqBNb4HTUIB3huwbIfUIC2nOpnHqXjNnkfiYb9fiESqF/bYG7RtZj+s+7SX5M+L7
XBxSdNzhWsoUaezXbpcz38dEygPzj1h6QJOTmhDJ7ytKX8Hj/+FvwHN/XffMCwCFHRCjzxhfiWzk
pYAfiOmIZ8IxGP2AhRErmAkWvxlDZEljComfwIA/wscjg==X02dp
```

To test a type mismatch error, do the following:

1.  Enter a new HOST application license:

    **10 user starter non-eval HOST application license, expires in three hours**

    ``` javascript
    AAABiQ0ODAoPeNp1kk9TwjAQxe/9FJnxXKYpeoCZHqCtgsqfgaIO4yWELURD0tm0KN/eWOjYdvD68
    vbtb3dzM9GKTBgS2iOU9n3a7/pkHiXE96jvbNhho3XnWXBQBuKtyIVWQTxN4sV8MV7GTirMHk5QO
    ZJTBsG91eITvPdJBEeQOgN0uNRHwIYtLKWGa1ocNoCzdGUATUA9h2uVdhjPxRGCHAtw5gXyPTMQs
    RwCn1Lf9XzXv3NqwVN2gGCZDBYWstLj70zgqSyad0fVWPXgJaClGUfB8KGXuG+rl1v3ab0euUOPv
    jofAlmD/XG8GJBY5YAZCtMa9Ze5MagVZAGKX/FVE4eyMDZtqrdgAq+19zJlWEr/Na0TXjkTx4KLj
    WzeKbyIjaAJE7aDYpa2tTSO+mvbCrBKo/ryate4Up9KfylnhjumhGEl0SCXzBjB1B9Q/QYhQulrH
    /fcue6svl1di8BwFFnZKAGTE3mGIalGksliJxTZVqTmvLF6fXxksjhzpkwaqP5s3fMDBMYhRDAtA
    hUAhcR3uL05YCxbclq7h1dNa+Nc+j4CFBrdN005oVlMN9yBlWeM4TlnrOhqX02j3
    ```

2.  Enter the plugin license:

    **10 user license, expires in three hours**

    ``` javascript
    AAABCQ0ODAoPeNpdjzFPwzAUhHf/CktsSKnsUJBayUNDorYC0og6gLq9mpfUIrEj2wn031MaYOh4d
    7pPd1fy0NONCpRNKedzxub8jhappDHjMUnRK6e7oK0REn2gjVZoPNLKOto1fa0NfccBG9uh8yTv2
    z26TVX6kxKckXuH8FNOIaCIOY8jNos4J9kATX9ORAWNRzKy/OR6AiroAUVwPZIn0CagAaMw++q0O
    545xc2KbFwNRvsRsQgNeK/BkC26Ad06FclyJqO38mUaPex2qyhh/JU8jttPaWk+jP00f84F+9eVx
    w5zaFHIbCvX+fL/XQLt3tpnbG3ARY0meBHfkqJ36gAeL69+A7mJdwYwLAIUOvp5LMC8ygBti6ue+
    ae0PwoyX8QCFBfM5Yqqy2BCrwExOXHECJskOitAX02dh
    ```

    You enter the plugin license *after* entering the host license - otherwise the previous plugin license is cached as "valid" for a short time.

## Testing Data Center compatibility

To test licensing for your Data Center compatible add-on, set your host application (e.g. JIRA) license to the following:

**3 Hour Expiration for Data Center**

``` javascript
AAABXg0ODAoPeNpdkU9vgkAQxe/7KTbpGQP+idWEg8Kmai0SRNuYXrYw6ra4kNldWr99CdQGOe7Mm
/d7M/sQnw2dmRO1x9QZTe3H6WhCQz+mfdsZksBcPgA3x50CVK5jkySXxx5PtCjB1WiAhAaTM1fgc
w1u33GGlj22nBFZiwSkgvhaQMAv4Ppsz9abkEW3DvspBF7rsXCwIFvAEnDpu/OnSWy97fZD6/lwW
Fhz23klK4E8xDw1ie5VE9wDqRHucnxWkl4L6q6W0YwyqQELFArep9SHErK8ACRKc3X+dwJsLOrd2
hZeVcgMyOR+utbdLuNlRlUOQZ6CcgdNr2vczsZSoUUuXRbELAqj5ZaRFy4qreQVp3OUJmd7zT+X6
k47+SXzb9mYd4kbPHEpFK9RM51xpQSXxEOoS93P8kElKIpaHYPSNGsw9JgjLTJzEpKmt/1Vg2ynY
iXPTAOr379nRNC6MC0CFQCRfD2L4KCIFgXYxfK7bp7Q08LQGgIUPxTLLKX/hpQGHl5tAL/dUDRnT
Hg=X02h9
```

**Confluence 3 hour expiration**

``` javascript
AAABCQ0ODAoPeNpdUNFOg0AQfO9XkPhMc2Bs0yaXSMsl2iAQqa2v67noJbA0dwexf+/Joal9nZmd2
ZmbI74HO6AgZgFbrdntOl4GiUj3DogWM9lRPd92ZEFa8QSq4R3hAA0oQroH24AxCmguu9ZrnU4Ny
K3u0QMlaEuoc2iReyQFC1ski/pClimJZHB/PuEoTcVBZEUpnj1d6A8gZcCqjnjyG+s54f7pPVNDY
yZD96yLICCJ4uuk9NnFIi+X8cOsQj2gfkz5plrchYd8JcJqc0zC12qX+eO8b99QF/WLQW14xBiby
vRafoLB0etnoJBFYcz+VbhOu5xwrFY0jQryvxUngcaxwrXzN27wjQkwLQIUe9x9vLLNd9fUle/0U
O/G3mT4j6UCFQCDPOxJITv6OhbghOtduVBMsvVfGg==X02dl
```

 

## Testing Evaluation License from Scratch

If you have UPM 2.0 or later installed in your host application instance, you can search for and install an evaluation license from scratch. This kind of testing requires an account on <a href="http://my.atlassian.com" class="external-link">My Atlassian</a>.

1.  Start the host application.
2.  Navigate to the **Plugins** page.
3.  Choose the **Install** tab.
4.  Select the plugin to install.
5.  Press the **Try** button.
    This button is only available on Paid-via-Atlassian plugins.
    After you press the button, the system installs the plugin and then prompts you to get a license:
    ![]
6.  Press **Continue**.
7.  Enter your username and password if prompted.
8.  Follow the prompts to retrieve an evaluation license.

  [errors returned by the Plugin Licensing API]: /market/license-validation-rules-8946470.html
  []: /market/attachments/8946477/10584134.png

