---
title: Highlight Screenshot Cropped 24805553
aliases:
    - /market/-highlight-screenshot-cropped-24805553.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24805553
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24805553
confluence_id: 24805553
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Highlight Screenshot Cropped

580 x 330px PNG/JPG



