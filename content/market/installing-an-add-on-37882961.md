---
title: Installing an Add On 37882961
aliases:
    - /market/installing-an-add-on-37882961.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=37882961
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=37882961
confluence_id: 37882961
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Installing an Add-on

You can install add-ons using the add-on manager for Atlassian applications, the <a href="https://confluence.atlassian.com/display/UPM/Universal+Plugin+Manager+Documentation" class="external-link">Universal Plugin Manager (UPM)</a>. First, in the UPM you can register the add-on through the UI, similar to how an administrator does. You can also do so programmatically do so against UPM's REST API. 

After registration, the add-on appears in the list of user-installed add-ons in the <a href="https://confluence.atlassian.com/display/UPM/Universal+Plugin+Manager+Documentation" class="external-link">Manage Add-ons</a> page, and its features are available for use in the target application.
 

This document has Atlassian Cloud applications as it's focus, however the steps also apply for Atlassian Server and Data Center software.

Installing an add-on using the Universal Plugin Manager

Installing your add-on adds it to your Atlassian Cloud application. To be more precise, installing is really just registering the add-on with the application and the only thing that is stored by the application at this time is the add-on descriptor. 

You can install an add-on with the UPM as follows. Note, these instructions were written for UPM version 2.14 or later.

1.  Log in to the Atlassian application interface as an admin or a system administrator. If you started the application with Atlassian's SDK, the  default username/password combination is `admin`/`admin`.
2.  Choose **![] &gt; Add-ons** from the menu. The Administration page will display.
3.  Choose the **Manage add-ons** option.
4.  Scroll to the page's bottom and click the **Settings** link. The **Settings** dialog will display. 
5.  Make sure the "Private listings" option is checked and click **Apply**.
6.  Scroll to the top of the page and click the **Upload Add-on** link.
7.  Enter the URL to the hosted location of your plugin descriptor. In this example, the URL is similar to the following:  <a href="http://localhost:8000/atlassian-plugin.xml" class="uri" class="external-link">http://localhost:8000/atlassian-plugin.xml</a>. (If you are installing to a cloud site, the URL must be served from the Marketplace, and will look like <a href="https://marketplace.atlassian.com/download/plugins/com.example.add-on/version/39/descriptor?access-token=9ad5037b" class="uri" class="external-link">https://marketplace.atlassian.com/download/plugins/com.example.add-on/version/39/descriptor?access-token=9ad5037b</a>)
8.  Press **Upload**. The system takes a moment to upload and register your plugin. It displays the **Installed and ready to go** dialog when installation is complete.  

<img src="/market/attachments/37882961/37882962.jpg" height="250" />

9. Click **Close.**

10. Verify that your plugin appears in the list of **User installed add-ons**. For example, if you used Hello World for your plugin name, that will appears in the list.

## Installing an add-on using the REST API

You can also install an add-on using the UPM's REST API. You'll find this method useful if you want to install add-ons programmatically, say from a script, or simply want to quickly install an add-on from the command line. Broadly speaking, installing an add-on (or performing any operation against the REST API of the UPM) is a two-step process:

First get a UPM token.

Next, issue the request to the REST API, including the token you received.

The following steps walk you through these steps in detail:

Send a GET request to the following resource:
`http://HOST_NAME:PORT/CONTEXT/rest/plugins/1.0/?os_authType=basic`
In your request:

1.  Replace `HOST_NAME` and `PORT` with the actual host name and port of the target Atlassian application. If applicable (i.e., if using a development instance), include the `CONTEXT` with the application context (`/jira` or `/confluence`).
2.  Include the username and password for a system administrator user in the target Atlassian application as HTTP Basic Authentication credentials.
3.  Set the Accept header for the request to: "`application/vnd.atl.plugins.installed+json`"

Capture the header named "upm-token" in the response. This is the UPM token you need for the next request.

Now install your add-on by issuing a POST request to the following resource:

`http://HOST_NAME:PORT/CONTEXT`/rest/plugins/1.0/?token=${upm-token}
In your request:

1.  Again use the actual host name and port and path for your target Atlassian application.

2.  The token value should be the value of the `upm-token` header you just received.

3.  In the request, set the Accept header to: "`application/json`"

4.  Set the Content-Type for the data to: "`application/vnd.atl.plugins.install.uri+json`"

5.  In the body of the POST, include the following JSON data:

    `{"pluginUri":"${plugin-xml-url}", "pluginName":"the plugin name"}`
    Replace `plugin-xml-url` with the hosted location of your descriptor file, and "the plugin name" with the name by which you want the add-on to be registered in the application.

This registers the add-on declared by the `atlassian-plugin.xml` file at the URL.

Note that you should not rely on the response returned from the POST request to confirm that the plugin has been installed. Instead, the best way to confirm that the plugin has been installed is to add a webhook to your add-on descriptor that listens for the add-on installation event. The webhook declaration in the `atlassian-plugin.xml` file would look something like this:

``` javascript
<webhook key="installed" event="remote_plugin_installed" url="/your-url-here" />
```

## Troubleshooting authentication

When registering from the command line using <a href="http://curl.haxx.se/docs/manpage.html" class="external-link">cURL</a>, keep in mind that cURL does not perform session maintenance across calls (unlike other clients, such as Apache HttpClient). Thus, you need to either:

Send the authentication credentials in both requests, or

Have cURL save any cookies from the first request and send them in the second. That is:

1.  To save cookies, use the -c switch: `curl -c cookiesfile.txt ...`
2.  And then include the cookies in the second request: `curl -b cookiesfile.txt ...`

**On this page:
**

-   [Installing an add-on using the REST API]
-   [Troubleshooting authentication]

  []: /market/attachments/5242881/21528778.png
  [Installing an add-on using the REST API]: #installing-an-add-on-using-the-rest-api
  [Troubleshooting authentication]: #troubleshooting-authentication

