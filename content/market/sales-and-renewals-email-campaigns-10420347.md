---
title: Sales and Renewals Email Campaigns 10420347
aliases:
    - /market/sales-and-renewals-email-campaigns-10420347.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10420347
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10420347
confluence_id: 10420347
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Sales and renewals email campaigns

If your add-on is a paid-via-Atlassian listing, it is automatically subject to the services of Atlassian's automated sales and renewal system.

This system is developed, tested, and maintained by Atlassian.

## When do we send emails?

We send automated emails to your evaluators or customers according to the following schedule:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Stage</p></th>
<th><p>Time</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Pre-Sale (quotes only)</p></td>
<td><p>After quote is created</p></td>
</tr>
<tr class="even">
<td><p>Pre-Sale (quotes only)</p></td>
<td><p>7 days before quote expiration date</p></td>
</tr>
<tr class="odd">
<td><p>Purchase</p></td>
<td><p>After every payment</p></td>
</tr>
<tr class="even">
<td><p>Maintenance</p></td>
<td><p>7 months after purchase date</p></td>
</tr>
<tr class="odd">
<td><p>Maintenance</p></td>
<td><p>90 days before expiration date</p></td>
</tr>
<tr class="even">
<td><p>Maintenance</p></td>
<td><p>45 days before expiration date</p></td>
</tr>
<tr class="odd">
<td><p>Maintenance (auto-renewal only)</p></td>
<td><p>30 days before expiration date</p></td>
</tr>
<tr class="even">
<td><p>Maintenance</p></td>
<td><p>15 days before expiration date</p></td>
</tr>
<tr class="odd">
<td><p>Maintenance (if not already renewed)</p></td>
<td><p>15 days after expiration date</p></td>
</tr>
</tbody>
</table>

## What do the emails look like?

The email messages use data from the customer database to populate each form. An example of a pre-sale, purchase, and maintenance email are shown below.

### Pre-sale email

<img src="/market/attachments/10420347/38436947.png" width="500" /> 

### Purchase email

<img src="/market/attachments/10420347/38436946.png" width="500" /> 

### Maintenance reminder email

<img src="/market/attachments/10420347/38436948.png" width="500" />

## Online Sales Report

You can view sales data for your add-ons via the [Sales Report API] or through the [vendor dashboard]. 

  [Sales Report API]: /market/accessing-sales-reports-with-the-rest-api-13633048.html
  [vendor dashboard]: https://developer.atlassian.com/display/MARKET/Reports+for+Paid-via-Atlassian+Listings

