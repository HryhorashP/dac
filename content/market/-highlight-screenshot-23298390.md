---
title: Highlight Screenshot 23298390
aliases:
    - /market/-highlight-screenshot-23298390.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=23298390
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=23298390
confluence_id: 23298390
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Highlight Screenshot

1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images



