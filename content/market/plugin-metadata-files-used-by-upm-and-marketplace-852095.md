---
title: Plugin Metadata Files Used By Upm and Marketplace 852095
aliases:
    - /market/plugin-metadata-files-used-by-upm-and-marketplace-852095.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=852095
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=852095
confluence_id: 852095
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Plugin metadata files used by UPM and Marketplace

When you submit your add-on for approval to list on the Marketplace, you provide a JAR file that contains your code and visual assets. The Marketplace parses your JAR, verifying it contains required metadata for approving and listing your add-on. This file is also used by the Universal Plugin Manager (UPM) to ensure your add-on is compatible with Atlassian host applications like JIRA or Confluence. 

This page describes key metadata properties and files used by UPM and the Marketplace.

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th>File</th>
<th>Purpose</th>
<th>Assets described or defined</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Descriptor file</p>
<p><code>atlassian-plugin.xml</code></p></td>
<td><p>Describes your add-on and marketing material to Atlassian host applications and UPM.</p>
<p><a href="https://developer.atlassian.com/x/KAAN">Reference SDK documentation</a> or <a href="#PluginmetadatafilesusedbyUPMandMarketplace-descriptor">read more below</a>.</p></td>
<td><ul>
<li>Plugin component modules</li>
<li>Vendor icon</li>
<li>Vender logo</li>
<li>Add-on banner</li>
<li>Add-on logo</li>
<li>In-application visual assets (icons for macro menus, etc)</li>
</ul></td>
</tr>
<tr class="even">
<td><p>Visual marketing descriptor</p>
<p><code>atlassian-plugin-marketing.xml</code></p></td>
<td><p>Describes visual marketing assets to the Atlassian Marketplace. This file is only used for Atlassian Marketplace listings.</p>
<p><a href="#PluginmetadatafilesusedbyUPMandMarketplace-marketing-descriptor">Read more below</a>.</p></td>
<td><ul>
<li>Add-on logo</li>
<li>Add-on banner</li>
<li>Add-on highlight screenshots, including cropped versions</li>
<li>Additional add-on screenshots</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>pom.xml</p>
<p> </p></td>
<td><p>Defines add-on dependencies.</p>
<p><a href="https://developer.atlassian.com/x/CQEr">Reference SDK documentation</a> or <a href="#PluginmetadatafilesusedbyUPMandMarketplace-pom">read more below</a>.</p></td>
<td><ul>
<li>Vendor name</li>
<li>Vendor website</li>
<li>Add-on name</li>
<li>Add-on description</li>
</ul></td>
</tr>
<tr class="even">
<td><p>JAR Manifest</p>
<p><code>MANIFEST.mf</code></p></td>
<td><a href="#PluginmetadatafilesusedbyUPMandMarketplace-manif">Read more below</a>.</td>
<td><ul>
<li>The build date property</li>
</ul></td>
</tr>
</tbody>
</table>

## Key files in your submission

Your JAR submission should include the project object model file `pom.xml`, the `atlassian-plugin.xml` file to describe dependencies, and a `atlassian-plugin-marketing.xml` file, all in addition to the JAR manifest. When you use the SDK `atlas-create*` commands, you supply the command with parameter values, and the generator populates corresponding parameters in the `pom.xml`. The `atlassian-plugin.xml` descriptor file references these values, and the `atlassian-plugin-marketing.xml` describes your visual marketing assets to the Marketplace. The JAR manifest file contains properties key to your submission. 

As you write your code, you can edit the metadata in these files either manually or through the SDK generations tools. Values you specify include:

-   key
-   organization name
-   visual resources such as logos, icons, and screenshots
-   version

When you use the SDK to generate your project JAR, the generator creates a version of the descriptor file that is read by Atlassian systems. The generated JAR also gets a manifest with some key properties.

When you list an add-on with the Atlassian Marketplace, the Marketplace submission form asks you to enter many of the same values again. The submission form also asks you for a deployable JAR. Both the Marketplace and UPM use a combination of your submission values and your metadata files to display your listing to your customers. To give your customers a consistent experience with your listing in the Marketplace and UPM, make sure the values you provide in your metadata files and your submission are the same.

## The `pom.xml` file

The `pom.xml` file contains several parameters that describe your plugin. The SDK's code generation commands populate these parameters or you can edit them manually. The `atlassian-plugin.xml` file references these parameters. The `pom.xml` values that identify your add-on and organization appear at the top of the POM:

``` javascript
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.example.plugins.tutorial</groupId>
    <artifactId>tutorial-plugin-marketing</artifactId>
    <version>1.0-SNAPSHOT</version>

    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>

    <name>tutorial-plugin-marketing</name>
    <description>This is the com.example.plugins.tutorial:tutorial-plugin-marketing plugin for Atlassian JIRA.</description>
    <packaging>atlassian-plugin</packaging>
```

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Field</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>groupId</code></p></td>
<td><p>This is typically a identifier that is unique to an organization or project. It may or may not use a dot format, such as <code>com.atlassian.plugins.tutorial</code>.</p></td>
</tr>
<tr class="even">
<td><p><code>artifactId</code></p></td>
<td><p>A descriptive identifier for your add-on. Together with the <code>groupId</code>, this value forms your plugin key. </p></td>
</tr>
<tr class="odd">
<td><p><code>version</code></p></td>
<td><p>The version of your add-on. This can be no more than 5 numbers. The version number must match what you specify on the Marketplace submission form.</p></td>
</tr>
<tr class="even">
<td><p><code>organization.name</code></p></td>
<td><p>The <code>name</code> is the name of your organization as it appears in the Atlassian Marketplace.</p></td>
</tr>
<tr class="odd">
<td><p><code>organization.url</code></p></td>
<td><p>The <code>url</code> is the your organization website location.</p></td>
</tr>
<tr class="even">
<td><p><code>name</code></p></td>
<td><p>Your add-on name. Your add-on name should be 40 characters or less, and <strong>must not</strong> contain any of the following words or names:</p>
<ul>
<li><ul>
<li>Atlassian</li>
<li>plugin</li>
<li>plug-in</li>
<li>add-on</li>
<li>addon</li>
<li>free</li>
<li>paid</li>
<li>Your vendor name</li>
</ul></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><code>description</code></p></td>
<td><p>A short description of your add-on.</p></td>
</tr>
</tbody>
</table>

The fields that identify which product versions your add-on supports are near the bottom in the `properties` stanza:

``` javascript
<properties>
    <amps.version>3.7.2</amps.version>
    <confluence.data.version>3.5</confluence.data.version>
    <confluence.version>4.0</confluence.version>
</properties>
```

In this example, the `confluence.version` value marked as compatible with the appropriate product version, i.e. the product versions in which UPM can manage your plugin.

## The `atlassian-plugin.xml` descriptor file

Describes a plugin and the modules contained within it. More complex plugins have complex descriptor files. There are specific attributes and `param` elements contained in the descriptor file that are important to your submission. You can use these `param` elements to define your marketing assets. You can also use `param` elements to define additional configuration or support pages in your plugin. The following example shows how to do this:

``` javascript
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />

        <param name="plugin-icon">images/pluginIcon.jpg</param>
        <param name="plugin-logo">images/pluginLogo.jpg</param>
        <param name="plugin-banner">images/pluginBanner.jpg</param>
        <param name="vendor-icon">images/vendorIcon.jpg</param>
        <param name="vendor-logo">images/vendorLogo.jpg</param>
    </plugin-info>
    ...

</atlassian-plugin>
```

The following table lists important metadata attributes or elements in this file:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute/element</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>References and concatenates of the <code>groupId</code> and <code>artifactId</code> from the <code>pom.xml</code>.</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>References the add-on <code>name</code> from the <code>pom.xml</code>.</p></td>
</tr>
<tr class="odd">
<td><p>description</p></td>
<td><p>References the <code>description</code> from the <code>pom.xml</code>.</p></td>
</tr>
<tr class="even">
<td><p>version</p></td>
<td><p>References the <code>version</code> from the <code>pom.xml</code>.</p></td>
</tr>
<tr class="odd">
<td><p>vendor.name</p></td>
<td><p>References the <code>organization.name</code> from the <code>pom.xml</code>.</p></td>
</tr>
<tr class="even">
<td><p>vendor.url</p></td>
<td><p>References the <code>organization.url</code> from the <code>pom.xml</code>.</p></td>
</tr>
</tbody>
</table>

The following table lists marketing-related `param` elements for your file:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Parameter Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>vendor-icon</code></p></td>
<td><p>Specifies a smaller version of your vendor logo. This appears in the sidebar and top-ranked lists. If your logo does not scale down well, you may create an additional thumbnail image.<br />
<strong>Length/Size</strong>: 16x16px PNG/JPG/GIF (Animated GIFs are not allowed.)</p></td>
</tr>
<tr class="even">
<td><p><code>vendor-logo</code></p></td>
<td><p>Specifies a large, clear, front-facing image. The logo appears on the add-on detail page and potentially in our featured tab. The logo should be relevant to your vendor name.<br />
<strong>Length/Size</strong>: 72x72px PNG/JPG/GIF (Animated GIFs are not allowed.)</p></td>
</tr>
<tr class="odd">
<td><p><code>plugin-logo</code></p></td>
<td><p>Specifies a large, clear, front-facing logo. This logo appears on the add-on detail page and potentially on our featured tab. We strongly recommend a chiclet-style background for the logo <a href="https://developer.atlassian.com/download/attachments/23298719/HighlightScreenshot920x450.psd?api=v2">(check out this free Photoshop template)</a>, as this will work best with upcoming changes to the user interfaces of UPM and the Atlassian Marketplace.<br />
<strong>Length/Size</strong>: 144 x 144px PNG/JPG</p></td>
</tr>
<tr class="even">
<td><p><code>plugin-banner</code></p></td>
<td><p>Specifies a catchy but relevant add-on banner, complete with a brief marketing message outlining what your add-on does, attractive visuals, and the full name of your company and add-on. If your add-on has a high number of downloads, this banner may be displayed on the Atlassian Marketplace homepage in the future. Do not include temporary promotion offers here.<br />
<strong>Length/Size</strong>: 1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</p></td>
</tr>
</tbody>
</table>

Provide these marketing assets as `resources` within your code. For example, for a `plugin-icon` path of `images/pluginIcon.gif`, you place your image within in the `src/main/resources/images/pluginIcon.gif` folder.

The following table lists url-related `param` elements for your file:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Parameter Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>configure.url</code></p></td>
<td><p>Specify internal links within the add-on that allow users to configure options for the plugin. Supported by all types of add-ons.</p></td>
</tr>
<tr class="even">
<td><p><code>post.install.url</code></p></td>
<td><p>Specifies a page that appears after add-on installation finishes. This can be identical to the <code>configure.url</code> parameter or not. For example, this page can contain getting started information for your plugin. Support only for Marketplace-place paid add-ons.</p></td>
</tr>
<tr class="odd">
<td><p><code>post.update.url</code></p></td>
<td><p>Specifies a page that appears after an add-on update finishes. For example, you could use this page to deliver &quot;What's New in this Version&quot; information to a customer. Support only for Marketplace-place paid add-ons.</p></td>
</tr>
</tbody>
</table>

If you supply one of the URL `param` elements, keep in mind the URL represents a servlet or action module defined in your plugin code. The URL value is the module's location relative to the host application's base URL. For example, if your configuration options are edited through a servlet in JIRA, your specify a URL relative to a base URL similar to this `http://myhost.local:2990/jira/plugins/servlet`.

A plugin can have both a `post.install.url` and a `post.update.url`. UPM displays the `post.install.url` when the plugin is first installed. The `post.update.url` only appears when the plugin is updated. UPM only uses these parameters if the user installed the plugin using the **Free Trial** or **Buy Now** buttons or updated it using the **Update** button. If a customer downloads an add-on JAR and uses the **Upload Plugin** installation technique, UPM ignores these parameters.

UPM always uses the `configure.url` parameter regardless of how a user installs a plugin.

**post URL Parameter Support in UPM**

The `post.install.url` and `post.update.url` require UPM 2.3 or higher. If a plugin defines these URLs but is installed or updated within a UPM 2.2 instance or earlier, they are ignored.

## The `atlassian-plugin-marketing.xml` file

This file is used only for marketing resources on Atlassian Marketplace listings. You can create this file under `src/main/resources` in your add-on JAR to reference visual assets used on the Atlassian Marketplace. This file should reference images in an `images` directory for your add-on JAR, like the following: 

``` javascript
<atlassian-plugin-marketing>
       
      <!--Describe names and versions of compatible applications -->
      <compatibility>
        <product name="jira" min="4.0" max="4.4.1"/>
        <product name="bamboo" min="3.0" max="3.1"/>
      </compatibility>
       
      <!-- Describe your add-on logo and banner. The banner is only displayed in the UPM. -->
      <logo image="images/01_logo.jpg"/>
      <banner image="images/02_banner.jpg"/>
       
      <!-- Describe highlight and cropped highlight image assets, in order. -->
      <highlights>
        <highlight image="images/03_highlight1_ss.jpg" cropped="images/04_highlight1_cropped.jpg"/>
        <highlight image="images/05_highlight2_ss.jpg" cropped="images/06_highlight2_cropped.jpg"/>
        <highlight image="images/07_highlight3_ss.jpg" cropped="images/08_highlight3_cropped.jpg"/>
      </highlights>
       
      <!-- Describe additional screenshots you'd like listed on your add-on listing. -->
      <screenshots>
        <screenshot image="images/09_addl_ss1.jpg"/>
        <screenshot image="images/10_addl_ss2.jpg"/>
        <screenshot image="images/11_addl_ss3.jpg"/>
      </screenshots>
  
</atlassian-plugin-marketing>
```

When you upload your plugin for approval, the Marketplace parses this file for assets to display on your add-on details page. The below assets can be packaged in your JAR. To view complete branding assets required, see [Branding your add-on].

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Parameter Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>plugin-logo</code></p></td>
<td><p>Specifies a large, clear, front-facing logo. This logo appears on the add-on detail page and potentially on our featured tab. We strongly recommend a chiclet-style background for the logo (<a href="https://developer.atlassian.com/download/attachments/23298719/HighlightScreenshot920x450.psd?api=v2">check out this free Photoshop template</a>), as this will work best with upcoming changes to the user interfaces of UPM and the Atlassian Marketplace.<br />
<strong>Size</strong>: 144 x 144px PNG/JPG</p></td>
</tr>
<tr class="even">
<td><p><code>plugin-banner</code></p></td>
<td><p>Specifies a catchy but relevant add-on banner, complete with a brief marketing message outlining what your add-on does, attractive visuals, and the full name of your company and add-on. If your add-on has a high number of downloads, this banner may be displayed on the Atlassian Marketplace homepage in the future. Please do not include temporary promotion offers here.<br />
<strong>Size</strong>: 1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</p></td>
</tr>
<tr class="odd">
<td><code>compatibility</code></td>
<td>Specifies the product name, minimum and maximum product compatibilities.  </td>
</tr>
<tr class="even">
<td><code>highlights</code></td>
<td><p>Specify each highlight with references to screenshots and cropped versions of each. List these references in order - the first listed will correspond to the first highlight.</p>
<p><strong>Size for each</strong> <strong><code>image</code>: </strong>1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</p>
<p><strong>Size for each <code>cropped</code> screenshot: 580 x 330px PNG/JPG</strong></p></td>
</tr>
<tr class="odd">
<td><code>screenshots</code></td>
<td><p>Specify up to five additional screenshots.</p>
<p><strong>Size:</strong> 1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</p></td>
</tr>
</tbody>
</table>

 

## The JAR manifest

Except in advanced situations, you should not need to edit or alter the `META-INF/MANIFEST.MF` file within your JAR. The Atlassian Plugin SDK generates the minimum necessary information in the manifest for you. If you do edit the manifest for any reason, be aware that it contains OSGI instructions that are necessary for proper functioning within the plugin framework; see [OSGi, Spring and the Plugin Framework].

Finally, the SDK commands generate an Atlassian-specific property in the JAR manifest -- the `Atlassian-Build-Date` property. This is the date that the JAR was built, and it has a format of: `yyyy-MM-ddTHH:mm:ssZ`. The following example illustrates a generated manifest with this property:

    Manifest-Version: 1.0
    Archiver-Version: Plexus Archiver
    Created-By: Apache Maven
    Built-By: manthony
    Build-Jdk: 1.6.0_31
    Atlassian-Build-Date: 2012-05-08T17:01:14-0700

If your add-on uses the Atlassian licensing API, make sure you do not remove the `Atlassian-Build-Date` property. The licensing system uses the property to determine whether a new version of a plugin is too new to be supported by an existing license. If you find that this property is not appearing in your manifest, you may be using an obsolete version of the Atlassian SDK.

 

  [Reference SDK documentation]: https://developer.atlassian.com/x/KAAN
  [read more below]: #read-more-below
  [Read more below]: #read-more-below
  [1]: https://developer.atlassian.com/x/CQEr
  [2]: #2
  [3]: #3
  [(check out this free Photoshop template)]: https://developer.atlassian.com/download/attachments/23298719/HighlightScreenshot920x450.psd?api=v2
  [Branding your add-on]: /market/branding-your-add-on-9699545.html
  [OSGi, Spring and the Plugin Framework]: https://developer.atlassian.com/display/DOCS/OSGi%2C+Spring+and+the+Plugin+Framework

