---
title: Search Results and Rankings 13632231
aliases:
    - /market/search-results-and-rankings-13632231.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632231
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632231
confluence_id: 13632231
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Search results and rankings

Making sure your add-on is discoverable in the Marketplace is important. This page outlines some basics about how the Marketplace rates and ranks search results. 

## Ordering

We index add-on listing fields like your vendor name, add-on name, summary, description, and categories. The biggest boost on search results comes from exact name matches, but stemmed search matching provides additional relevancy. 

We also perform a "fuzzy search" on add-on and vendor name (only), but it's weighed less than regular matches on other fields. This allows people to search for "conflunce" and still get "Confluence" search results.

For a more detailed search relevancy score, you can explore how Lucene works. The scoring formula is described in detail here: <a href="http://lucene.apache.org/core/3_6_0/api/core/org/apache/lucene/search/Similarity.html" class="uri" class="external-link">http://lucene.apache.org/core/3_6_0/api/core/org/apache/lucene/search/Similarity.html</a>

## Staff-picked add-ons

You might wonder how we choose add-ons to be part of the "staff-picked" category. Atlassian staff, mainly our Marketing team, frequently browses the add-on listings and staff-picks and promotes add-ons that are either very popular in terms of number of installations, positive ratings, or otherwise noteworthy add-ons, and chooses accordingly.

## Most popular rankings

When you filter search results by "most popular," we organize listings based on the total number of active installations. We look at how many product instances report having the add-on installed, and filter in descending order. In the future, we'll take customer ratings into account as well.

## Trending add-ons

This filter returns add-ons which gained the most active installations in the past week. These add-ons are sorted by the delta between new customers added and existing customers who uninstalled the add-on. We also omit Atlassian-developed add-ons so your add-on listings don't nave to compete with us for screen real estate.

## Top-selling add-ons

This filter sorts paid-via-Atlassian add-ons by total dollars in the last month (30 days). This filter also excludes Atlassian add-ons.

## Recently added add-ons

As you might expect, this sorts all add-ons (free, paid-via-vendor, paid-via-Atlassian) by the date the add-on was first approved for the Marketplace. We look at the approval date for the first public version of the add-on, not new versions of an add-on that already exists in the Marketplace.



