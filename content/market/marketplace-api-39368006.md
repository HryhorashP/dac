---
title: Marketplace API 39368006
aliases:
    - /market/marketplace-api-39368006.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368006
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368006
confluence_id: 39368006
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Marketplace API

The [Marketplace API] is a RESTful API that allows you to manage add-ons and your vendor account.

-   [Versioning]
-   [Authentication]
-   [URI structure]
-   [Request format]
-   [Pagination]
-   [Response format]
-   [Errors]
-   [Documentation for API version 1 resources]
-   [More information]

## Versioning

Version 2 is the latest release of the Marketplace API. Some version 1 resources remain available; for details see [Documentation for API version 1 resources].

## Authentication

The Marketplace API uses <a href="https://en.wikipedia.org/wiki/Basic_access_authentication" class="external-link">HTTP basic authentication</a> and your Atlassian account. Once authenticated, you can view and modify most properties of your add-ons and your account. If you are not logged in, you are considered an unauthenticated user and will not be able to edit add-ons or vendor information.

## URI structure

All URIs start with the *https://marketplace.atlassian.com/rest/2* prefix. When a resource provides a link to another resource, the link URI will not include the hostname (for instance, the link will be `/rest/2/addons` rather than `https://marketplace.atlassian.com/rest/2/addons`).

URIs referring to content that is not provided by Atlassian Marketplace are always absolute (that is, they use *http://* or *https://*) and are encoded as ordinary string properties.

## Request format

All request and response representations are in JSON and use the content type `application/json` with the UTF-8 character set, except for `PATCH` requests.

The Marketplace API uses standard HTTP methods. Successful `POST`, `PUT`, and `PATCH` requests return a `Location` header in addition to the HTTP status codes documented below. Successful `DELETE` requests return only the status code. See [Errors][1] for information about responses that return an error.

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th>Method</th>
<th>Description</th>
<th>Successful response</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>GET</code></td>
<td>All <code>GET</code> requests are idempotent</td>
<td>200</td>
</tr>
<tr class="even">
<td><code>POST</code></td>
<td>Creates a new entity</td>
<td>201</td>
</tr>
<tr class="odd">
<td><code>PUT</code></td>
<td>Modifies all editable properties of existing entity. It cannot be applied to a collection resource</td>
<td>204</td>
</tr>
<tr class="even">
<td><code>PATCH</code></td>
<td><p>Modifies only selected properties of an existing entity. It cannot be applied to a collection resource</p>
<p>Request bodies use the <a href="https://tools.ietf.org/html/rfc6902" class="external-link">JSON Patch</a> document structure with the content type <code>application/json-patch+json</code></p></td>
<td>204</td>
</tr>
<tr class="odd">
<td><code>DELETE</code></td>
<td>Removes an entity. It cannot be applied to a collection resource and accepts no request body</td>
<td>204</td>
</tr>
</tbody>
</table>

## Pagination

The Marketplace API uses pagination to limit response size for resources that may return large numbers of results. HTTP clients may specify a smaller page size of *N* by adding the query parameter `limit=N`. A client may also specify a starting offset for a paginated resource by adding the query parameter `offset=N`, meaning that the first *N* items are skipped. Non-paginated resources  ignore the `limit` and `offset` parameters:

-   `offset` Optional. Specifies the number of items to skip. The default value is 0.
-   `limit` Optional. Specifies the maximum page size, between 0 and 50, with a default value of 10. Setting `limit=0` returns only the overall properties of a collection resource, such as the total number of available items, without getting the properties of any individual item.

If the `limit` argument fails to validate, the API may respond with an error like the example below:

``` javascript
{
    errors: [
        {
            message: "limit: Must be an integer"
        }
    ]
}
```

## Response format

All resources containing links to Atlassian Marketplace resources and web pages use the <a href="https://en.wikipedia.org/wiki/Hypertext_Application_Language" class="external-link">HAL specification</a> with JSON (with some extensions as described below under `POST` and `PUT`, for cases that HAL does not cover). Such links will always be within a top-level `_links` or `_embedded` object. External links provided by third parties are considered simple data properties and can appear anywhere in a representation.

All resource links should:

-   be relative to the host root, for example, `/rest/2/addons`
-   have a `self` link referring to themselves

Links to the Marketplace website may be absolute or relative URIs. External links must always be absolute URIs.

When available, paginated responses provide the `next` and `prev` links to retrieve the next or previous page of items:

-   `next` Retrieves the next page of items.

<!-- -->

-   `prev` Retrieves the previous page of items.

## Errors

The following response status codes may be accompanied by an error representation.

| Status | Description                                                         |
|--------|---------------------------------------------------------------------|
| 400    | Request body is malformed or has an illegal property value          |
| 401    | Authentication is required                                          |
| 403    | User was authenticated but is not authorized for this operation     |
| 404    | Resource does not exist or is not visible to this user              |
| 409    | Resource cannot be created or modified due to some state constraint |
| 500    | Unexpected internal error in the Atlassian Marketplace              |
| 502    | Unexpected error in an external service                             |

Error responses may contain a JSON representation with a single top-level property, `errors`, that contains an array of objects (each with a message) and optional `path` and `code` properties. The `path` uses <a href="http://tools.ietf.org/html/rfc6901" class="external-link">JSON Pointer</a> format to refer to an invalid field in the request representation. If `path` is not present, the message describes a general condition that isn't tied to a specific field. If present, the `code `is a string that uniquely identifies the type of error. An example appears below:

``` javascript
{
    "errors": [
        { "message": "There is a general problem." },
        { "path": "/name", "message": "cannot be longer than 5000 characters" }
    ]
}
```

## Documentation for API version 1 resources

Version 1 of the Marketplace API remains available and some resource documentation is available.

-   [Sales Report API]. This resource is also available in API version 2 ([latest documentation])
-   [License Service API]. This resource is only available in API version 1.

If you are using the [License Service API], then you need to be approved for access by the Atlassian Marketplace team. This is necessary because the Atlassian Marketplace team audits usage and monitors the performance of the API very closely, subject to the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Atlassian Marketplace Vendor Agreement</a>.

 

## More information

 

Refer to the following pages for more information about the latest API version.

-   [Marketplace API reference]
-   [Accessing Sales Reports with the REST API][Sales Report API]
-   [Examples of API usage through JSON requests]
    -   [Creating an add-on version using JSON]
    -   [Getting add-on details using JSON]
    -   [Searching for add-ons using JSON]
-   [Using the Marketplace API Java client]
    -   [Create an add-on version from Java]
    -   [Get add-on details from Java]
    -   [Search for add-ons from Java]

  [Marketplace API]: https://developer.atlassian.com/market/api/2/reference/
  [Versioning]: #versioning
  [Authentication]: #authentication
  [URI structure]: #uri-structure
  [Request format]: #request-format
  [Pagination]: #pagination
  [Response format]: #response-format
  [Errors]: #errors
  [Documentation for API version 1 resources]: #documentation-for-api-version-1-resources
  [More information]: #more-information
  [1]: #1
  [Sales Report API]: /market/accessing-sales-reports-with-the-rest-api-13633048.html
  [latest documentation]: https://developer.atlassian.com/market/api/2/reference/resource/vendors/%7BvendorId%7D/reporting
  [License Service API]: /market/converting-proprietary-licenses-into-marketplace-licenses-10421889.html
  [Marketplace API reference]: /market/marketplace-api-reference-39999042.html
  [Examples of API usage through JSON requests]: /market/examples-of-api-usage-through-json-requests-39984261.html
  [Creating an add-on version using JSON]: /market/creating-an-add-on-version-using-json-39984274.html
  [Getting add-on details using JSON]: /market/getting-add-on-details-using-json-39984327.html
  [Searching for add-ons using JSON]: /market/searching-for-add-ons-using-json-39984318.html
  [Using the Marketplace API Java client]: /market/using-the-marketplace-api-java-client-39984232.html
  [Create an add-on version from Java]: /market/create-an-add-on-version-from-java-39984255.html
  [Get add-on details from Java]: /market/get-add-on-details-from-java-39984291.html
  [Search for add-ons from Java]: /market/search-for-add-ons-from-java-39984289.html

