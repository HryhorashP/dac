---
title: Atlassian Plugin.Xml Branding 24805609
aliases:
    - /market/-atlassian-plugin.xml-branding-24805609.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24805609
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24805609
confluence_id: 24805609
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_atlassian-plugin.xml branding

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th>Asset</th>
<th>Tag</th>
<th>Size of asset</th>
<th>Description of asset</th>
<th>Example reference to asset</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Vendor name &amp; URL</td>
<td><code>vendor</code></td>
<td><p>Vendor names can be up to 40 characters.</p>
<p> </p></td>
<td><p>Use your company name or, if an individual, your own name. Be mindful of trademark and copyright guidelines. Short names are preferred.</p></td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
{{< highlight javascript >}}&lt;vendor name=&quot;Atlassian Plugin Generator&quot; url=&quot;http://example.com&quot;/&gt;{{< /highlight >}}
</div>
</div></td>
</tr>
<tr class="even">
<td>Vendor logo</td>
<td><code>param name=&quot;vendor-icon&quot;</code></td>
<td>72x72px PNG/JPG/GIF</td>
<td>Reference a large, clear, front-facing image. Your logo should be relevant to your vendor name. This appears on your vendor page in the Marketplace.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
{{< highlight javascript >}}&lt;param name=&quot;vendor-logo&quot;&gt;images/vendorLogo.jpg&lt;/param&gt;{{< /highlight >}}
</div>
</div></td>
</tr>
<tr class="odd">
<td>Vendor icon</td>
<td><code>param name=&quot;vendor-icon&quot;</code></td>
<td>16x16px PNG/JPG/GIF</td>
<td>Reference a smaller version of your vendor logo. This appears in the sidebar and top-ranked lists. If your logo doesn't scale down well, create an additional thumbnail image. In the future, your logo may be scaled to be your vendor icon.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
{{< highlight javascript >}}&lt;param name=&quot;vendor-icon&quot;&gt;images/vendorIcon.jpg&lt;/param&gt;{{< /highlight >}}
</div>
</div></td>
</tr>
<tr class="even">
<td>Add-on logo</td>
<td><code>param name=&quot;plugin-logo&quot;</code></td>
<td>144 x 144px PNG/JPG</td>
<td>Reference logo with a transparent or bounded, chiclet-style background. Marketplace automatically creates an add-on icon from your logo.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
{{< highlight javascript >}}&lt;param name=&quot;plugin-logo&quot;&gt;images/pluginLogo.jpg&lt;/param&gt;{{< /highlight >}}
</div>
</div></td>
</tr>
<tr class="odd">
<td>Add-on banner</td>
<td><code>param name=&quot;plugin-banner&quot;</code></td>
<td>1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</td>
<td>Include your add-on name, your vendor name, and brief text about your add-on's functionality. Users see this banner displayed in the UPM when they browse the Marketplace from their Atlassian host product.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
{{< highlight javascript >}}&lt;param name=&quot;plugin-banner&quot;&gt;images/pluginBanner.jpg&lt;/param&gt;{{< /highlight >}}
</div>
</div></td>
</tr>
</tbody>
</table>



