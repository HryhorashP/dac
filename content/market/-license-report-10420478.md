---
title: License Report 10420478
aliases:
    - /market/-license-report-10420478.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10420478
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10420478
confluence_id: 10420478
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_License report

Your license report details user information for those that have started an evaluation or purchased a license for your paid-via-Atlassian add-on. This report is available through your Marketplace account. 

1.  **Log in** from the upper right corner of any page in the Marketplace.
2.  Click **Manage listings** from the upper right corner.
3.  Click **Sales and evals** to access your vendor dashboard.
4.  Click **Download license report**. 
    <img src="/market/attachments/10420478/27230291.png" title="Click sales and evals, then Download license" alt="Click sales and evals, then Download license" width="500" />
    The downloaded file is titled licenseReport.csv.

### **Data included in the license report**

The license report includes the following fields and columns: 

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Data</p></th>
<th><p>Description</p></th>
<th><p>Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Add-on license ID</p>
<p><code>licenseId</code></p></td>
<td><p>A unique value in Atlassian SEN format. Note that in Atlassian Cloud, the same license ID will be used for all products licensed for a given Atlassian Cloud instance. Evaluations have a license ID beginning with <code>SEN-L</code>, and that license ID will change once the customer purchases a license.</p></td>
<td><p>SEN-205346</p></td>
</tr>
<tr class="even">
<td><p>Organization name</p>
<p><code>organisationName</code></p></td>
<td><p>A string. This field is optional.</p></td>
<td><p>BMW AG</p></td>
</tr>
<tr class="odd">
<td><p>Add-on name</p>
<p><code>addOnName</code></p></td>
<td><p>A string representing the name of your add-on. This field is populated by the Marketplace.</p></td>
<td><p>GreenHopper for JIRA</p></td>
</tr>
<tr class="even">
<td><p>Add-on key</p>
<p><code>addOnKey</code></p></td>
<td>Your add-on key. If the sale is for a plugin in Atlassian Cloud, the key will end with <code>.ondemand</code>.</td>
<td>com.atlassian.plugins.jira.greenhopper</td>
</tr>
<tr class="odd">
<td><p>Technical contact name</p>
<p><code>technicalContactName</code></p></td>
<td><p>A string representing the contact name. This field is optional.</p></td>
<td><p>Alice Adams</p></td>
</tr>
<tr class="even">
<td><p>Technical contact email</p>
<p><code>technicalContactEmail</code></p></td>
<td><p>A valid email address. This field functions as a unique customer ID.</p></td>
<td><p>alice.adams@supercompany.com</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact phone number</p>
<p><code>technicalContactPhone</code></p></td>
<td><p>A phone number. This field is optional.</p></td>
<td><p>+61294811111</p></td>
</tr>
<tr class="even">
<td><p>Technical contact address 1</p>
<p><code>technicalContactAddress1</code></p></td>
<td><p>A string representing the address. This field is optional.</p></td>
<td><p>1 Spam Street</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact address 2</p>
<p><code>technicalContactAddress2</code></p></td>
<td><p>A string representing the address. This field is optional.</p></td>
<td><p>Suite 210</p></td>
</tr>
<tr class="even">
<td><p>Technical contact city</p>
<p><code>technicalContactCity</code></p></td>
<td><p>A string representing an city. This field is optional.</p></td>
<td><p>San Francisco</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact state</p>
<p><code>technicalContactState</code></p></td>
<td><p>A string representing a state. This field is optional.</p></td>
<td><p>CA</p></td>
</tr>
<tr class="even">
<td><p>Technical contact postal code/zip code</p>
<p><code>technicalContactPostcode</code></p></td>
<td><p>A string representing a postal code. This field is optional.</p></td>
<td><p>94103</p></td>
</tr>
<tr class="odd">
<td><p>Technical contact country</p>
<p><code>technicalContactCountry</code></p></td>
<td><p>A string representing a country. This field is optional.</p></td>
<td><p>US</p></td>
</tr>
<tr class="even">
<td><p>Billing contact name</p>
<p><code>billingContactName</code></p></td>
<td><p>A string representing the contact name. This field is optional.</p></td>
<td><p>Steve Peters</p></td>
</tr>
<tr class="odd">
<td><p>Billing contact email</p>
<p><code>billingContactEmail</code></p></td>
<td><p>A valid email address. This field is optional.</p></td>
<td><p>steven.peters@company.com</p></td>
</tr>
<tr class="even">
<td><p>Billing contact phone</p>
<p><code>billingContactPhone</code></p></td>
<td><p>A string representing the phone number. This field is optional.</p></td>
<td><p>+61294811111</p></td>
</tr>
<tr class="odd">
<td><p>Edition</p>
<p><code>edition</code></p></td>
<td>A string representing the user tier of the license.</td>
<td>10 Users</td>
</tr>
<tr class="even">
<td><p>Application license type</p>
<p><code>licenseType</code></p></td>
<td><p>The value of the Atlassian product license. This value is one of:</p>
<ul>
<li>Evaluation</li>
<li>Commercial</li>
<li>Academic</li>
<li>Open Source</li>
<li>Community</li>
</ul></td>
<td><p>Commercial</p></td>
</tr>
<tr class="odd">
<td><p>Add-one license start date</p>
<p><code>startDate</code></p></td>
<td><p>When the product was originally purchased. The format for this field is <code>YYYY-MM-DD</code>.</p></td>
<td><p>2013-02-04</p></td>
</tr>
<tr class="even">
<td><p>Add-on license end date</p>
<p><code>endDate</code></p></td>
<td><p>When the license is schedule to expire. The format for this field is <code>YYYY-MM-DD</code>.</p></td>
<td><p>2013-02-04</p></td>
</tr>
<tr class="odd">
<td><p>Renewal action</p>
<p><code>renewalAction</code></p></td>
<td><p>When the license expires, the sales system does one of the following:</p>
<ul>
<li>AUTO_QUOTE : automatically quoted 90 days prior to expiration</li>
<li>AUTO_RENEW : automatically renewed using the saved CC on the license expiration date.</li>
<li>NONE : a reminder is not sent to the customer</li>
</ul></td>
<td><p>AUTO_QUOTE</p></td>
</tr>
</tbody>
</table>



