---
title: Data Center Add On Development 28317214
aliases:
    - /market/data-center-add-on-development-28317214.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=28317214
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=28317214
confluence_id: 28317214
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Data Center add-on development

This page covers JIRA and Confluence Data Center add-on development for the Atlassian Marketplace. On this page, you'll find links to Confluence or JIRA-specific resources concerning add-on development and testing in a clustered environment. This page also provides instructions for listing your Data Center-compatible add-on in the Marketplace. 

## About JIRA and Confluence Data Center

JIRA and Confluence Data Center are specifically designed for high availability and performance at scale. Data Center instances run on multiple servers or nodes, and point back to a single relational database. For this reason, we refer to Data Center instances of JIRA and Confluence as 'clustered.'

Data Center versions of Confluence and JIRA are sold in 1000 user blocks as annual licenses. These licenses are not perpetual, and customers can apply the license key  to multiple (unlimited) nodes.

**What's here:**

-   [About JIRA and Confluence Data Center]
-   [Data Center add-on development resources]
    -   [JIRA Data Center add-on development]
    -   [Confluence Data Center add-on development]
-   [Develop future versions as Data Center compatible]
-   [Marketplace pricing for Data Center-compatible add-ons]
-   [How the Marketplace determines Data Center compatibility]
-   [Adding a Data Center-compatible add-on in the Marketplace]

## Data Center add-on development resources

### **JIRA Data Center add-on development**

-   [Configuring a clustered JIRA development environment]
-   [Developing a Data Center-compatible add-on for JIRA]
-   [Testing your Data Center-compatible add-on in JIRA]

### **Confluence Data Center add-on development**

-   [Configuring a clustered Confluence development environment]
-   [Developing a Data Center-compatible add-on for Confluence]
-   [Testing your Data Center-compatible add-on in Confluence]
-   <a href="https://confluence.atlassian.com/x/d-gC" class="external-link">Technical overview of Confluence clustering</a>

## Develop future versions as Data Center compatible

If you're starting with an add-on used in standard JIRA and Confluence Server instances, we recommend developing with Data Center compatibility in mind for **all future versions**. If you already maintain a standard add-on for JIRA or Confluence Server, you might be tempted to simply fork your code and develop separate versions. Instead, we recommend using a single set of source code for future add-on releases - if you develop your add-on following the above links, it should play well with standard JIRA and Confluence instances in addition to clustered Data Center instances. 

## Marketplace pricing for Data Center-compatible add-ons

If you invest significant research and development resources in creating a Data Center-compatible version of your add-on, you might raise the largest tier prices by a small amount. As you consider this option, keep in mind that all Marketplace visitors see the same pricing tables, regardless if they use JIRA Data Center clustered on multiple servers or JIRA running on a single server. Customers are billed based on their application license. 

Though JIRA and Confluence Data Center licenses are sold in 1000-user blocks, 1000-user pricing tiers for add-ons are not yet available.** **In the interim, customers can purchase a Server license for add-ons equal to (or greater than) their Data Center license. Customers can purchase 2000, 10,000, or 10,000+ user tiers. Just like JIRA and Confluence Data Center, customers can then apply these license keys to an unlimited number of nodes running their instance.

## How the Marketplace determines Data Center compatibility

When you list your first cluster-compatible add-on version in the <a href="https://marketplace.atlassian.com/" class="external-link">Marketplace</a>, modify your `atlassian-plugin.xml` descriptor file. This tells the Marketplace and UPM that your add-on is compatible with Data Center instances. Add the following parameter inside the `plugin-info` section:

``` xml
       <param name="atlassian-data-center-compatible">true</param>
```

Here's an example of a generic `plugin-info` block with this param:

``` xml
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="atlassian-data-center-compatible">true</param>
    </plugin-info>
```

## Adding a Data Center-compatible add-on in the Marketplace

After you've tested and checked your add-on in clustered environments, you're ready to add a new version of your add-on to the Marketplace. A few things to keep in mind before you list your new version:

-   **Standard versions and Data Center-compatible versions of your add-on share a single Marketplace listing**
    This means if you have a standard add-on for JIRA or Confluence, the Data Center-compatible version will be on the same add-on details page. Just like if your add-on offers the same functionality for different hosting models (Cloud and Server), Data Center and standard Cloud or Server add-ons share the same listing page.
-   **Your add-on is demarcated with a Data Center icon to convey compatibility**
    Your Marketplace listing reflects if your add-on is Data Center compatible. If your descriptor declares the `atlassian-data-center-compatible` param to be `true`, your listing will show a Data Center icon: 
    <img src="/market/attachments/28317214/28770737.png" title="Data Center compatibility icon" alt="Data Center compatibility icon" width="500" height="115" />

Here's how to add a new Data Center version of your add-on:

1.  <a href="https://marketplace.atlassian.com/login" class="external-link">Log in to the Marketplace</a>.

2.  Click ****Manage listings**** from the menu bar.

3.  Click the name of the add-on you're adding a Data Center-compatible version for.

4.  Click **Create version**.
    ![(info)] You should have already added `<param name="atlassian-data-center-compatible">true</param>` to your descriptor file.
5.  Make any corresponding changes in release notes or[ branding assets].

6.  Save your new version.
    If your add-on has already been approved, Data Center versions don't prompt another approval.

 

  [About JIRA and Confluence Data Center]: #about-jira-and-confluence-data-center
  [Data Center add-on development resources]: #data-center-add-on-development-resources
  [JIRA Data Center add-on development]: #jira-data-center-add-on-development
  [Confluence Data Center add-on development]: #confluence-data-center-add-on-development
  [Develop future versions as Data Center compatible]: #develop-future-versions-as-data-center-compatible
  [Marketplace pricing for Data Center-compatible add-ons]: #marketplace-pricing-for-data-center-compatible-add-ons
  [How the Marketplace determines Data Center compatibility]: #how-the-marketplace-determines-data-center-compatibility
  [Adding a Data Center-compatible add-on in the Marketplace]: #adding-a-data-center-compatible-add-on-in-the-marketplace
  [Configuring a clustered JIRA development environment]: https://developer.atlassian.com/x/lASbAQ
  [Developing a Data Center-compatible add-on for JIRA]: https://developer.atlassian.com/x/YaSkAQ
  [Testing your Data Center-compatible add-on in JIRA]: https://developer.atlassian.com/x/YaSkAQ#PluginGuidetoJIRAHighAvailabilityandClustering-Testingyourplugin
  [Configuring a clustered Confluence development environment]: https://developer.atlassian.com/x/tgWbAQ
  [Developing a Data Center-compatible add-on for Confluence]: https://developer.atlassian.com/x/kQAf
  [Testing your Data Center-compatible add-on in Confluence]: https://developer.atlassian.com/x/kQAf##HowdoIensuremyadd-onworksproperlyinacluster?-Testingyouradd-oninacluster
  [(info)]: /market/images/icons/emoticons/information.png
  [ branding assets]: https://developer.atlassian.com/x/2QCU

