---
title: License Validation Rules 8946470
aliases:
    - /market/license-validation-rules-8946470.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8946470
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8946470
confluence_id: 8946470
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : License Validation Rules

The UPM 2.0 licensing API checks add-on licenses for validity, and returns an error that add-ons can use to enforce license validity in a custom manner. The error conditions are listed below.

By default, an invalid license is enforced differently between standard and evaluation licenses, as follows:

-   Non-evaluation licenses have maintenance termination dates. When a maintenance term expires for a standard license, the plugin continues to work but cannot be updated to new versions of the plugin. Also, vendor support for the plugin in no longer available after the maintenance period ends.
-   Evaluation licenses have expiration dates. The plugin can be updated as long as the evaluation license is valid. However, unlike a standard license, the plugin stops working when the license period for an evaluation license ends.

**On this page:**

-   [Valid License]
-   [Conditions that Invalidate a Plugin License]
-   [Handling a License Validation Error]

## Valid License

A license is valid if it does not have any error conditions.

## Conditions that Invalidate a Plugin License

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Expired</p></td>
<td>A license is invalid if the current date is greater than or equal to the license's expiration date. Normally, only evaluation and subscription licenses have expiration dates. Perpetual licenses do <em>not</em> have expiration dates; instead, perpetual licenses have <em>maintenance expiration dates</em> after which the add-on should continue to be fully functioning, but the customer will no longer be eligible for add-on updates or support.<br />
<br />
<strong>Suggested Error Message:</strong><br />
<code>Invalid license: Your evaluation license of ADD_ON_NAME expired. Please use the 'Buy' button to purchase a new license.</code></td>
</tr>
<tr class="even">
<td><p>Type mismatch</p></td>
<td><p>A license is invalid if its type (commercial, developer, academic, etc.) is incompatible with the type of the application license:</p>
<ul>
<li>If the application license is <em>developer</em>, all plugin license types are compatible.</li>
<li>If the application license is <em>hosted</em>, the plugin license type must be <em>hosted, academic, commercial, community,</em> or <em>open source</em>. (This license type was used for hosted installations prior to On-Demand; it will likely become obsolete.)</li>
<li><p>For all other application license types, the plugin license type must be the same as the application license type.</p></li>
</ul>
<p>Starting with UPM 2.7, Enterprise license enforcement is also supported. Customers using UPM 2.7 or later will also have an incompatibility:</p>
<ul>
<li>If the application license is an <em>Enterprise</em> license and the plugin license is not.</li>
</ul>
<p><br />
<strong>Suggested Error Message:</strong><br />
<code>Invalid license: Your ADD_ON_NAME license does not match the LICENSE_TYPE license on this ATLASSIAN_APPLICATION_NAME installation. Please get a LICENSE_TYPE license for ADD_ON_NAME and try again.</code></p>
<div class="confluence-information-macro-note confluence-information-macro">
<p>Evaluation Licenses</p>
<div class="confluence-information-macro-body">
<p>The system skips this test for evaluation licenses and considers all license types compatible. This is true if either or both of the plugin license and application license is an evaluation license.</p>
</div>
</div></td>
</tr>
<tr class="odd">
<td><p>User mismatch</p></td>
<td><p>A license is invalid if its <em>maximum number of users</em> property is less than that of the application license:</p>
<ul>
<li>Application is licensed for an unlimited number of users; plugin license has a fixed user limit</li>
<li><p>Application is licensed for a fixed limit; plugin license has a fixed limit that is lower<br />
<br />
<strong>Suggested Error Message:</strong><br />
<code>Invalid license: Your ADD_ON_NAME is only licensed for LIMIT_VALUE users. Your ATLASSIAN_APPLICATION installation requires a license for LIMIT users. Please get a ADD_ON_NAME license for LIMIT_VALUE users and try again.</code></p>
<div class="confluence-information-macro-note confluence-information-macro">
<p>Evaluation Licenses</p>
<div class="confluence-information-macro-body">
<p>The system skips this test for evaluation licenses and considers all license types compatible. This is true if either or both of the plugin license and application license is an evaluation license.</p>
</div>
</div></li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong>Edition mismatch</strong></p></td>
<td><p>This error currently only applies to Bamboo plugin licenses. A license is invalid if its <em>maximum number of remote agents</em> property is less than that of the application license:</p>
<ul>
<li>Application is licensed for an unlimited number of remote agents; plugin license has a fixed remote agent limit</li>
<li><p>Application is licensed for a fixed limit; plugin license has a fixed limit that is lower<br />
<br />
<strong>Suggested Error Message:</strong><br />
<code>Invalid license: Your ADD_ON_NAME is only licensed for LIMIT_VALUE remote agents. Your ATLASSIAN_APPLICATION installation requires a license for LIMIT remote agents. Please get a ADD_ON_NAME license for LIMIT_VALUE users and try again.</code></p>
<div class="confluence-information-macro-note confluence-information-macro">
<p>Evaluation Licenses</p>
<div class="confluence-information-macro-body">
<p>The system skips this test for evaluation licenses and considers all license types compatible. This is true if either or both of the plugin license and application license is an evaluation license.</p>
</div>
</div></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>Version mismatch</p></td>
<td><p>A license is invalid if the plugin's build date is greater than or equal to the license's <em>maintenance expiration date</em>. That is, if the license says that support is only available through 1/1/2012, then an existing installation of a plugin version with a build date of 1/1/2011 will continue working indefinitely, but if you install a new version built on 1/2/2012 or later it will not work with the old license.<br />
<br />
<strong>Suggested Error Message:</strong><br />
<code>Invalid license: Your license for maintenance of ADD_ON_NAME is not valid for version X.Y.Z. Please use the 'Renew' button to renew your ADD_ON_NAME license.</code></p>
<p>The plugin build date is indicated by an Atlassian-specific property in the manifest of the plugin JAR, <code>Atlassian-Build-Date</code>. This property is added to the plugin manifest by the Plugins SDK. You should not remove the <code>Atlassian-Build-Date</code> property from the manifest.</p></td>
</tr>
</tbody>
</table>

## Handling a License Validation Error

You can implement your own license enforcement behavior through the `ThirdPartyPluginLicenseStorageManager` class. For example, an invalid license may cause your plugin to function in read-only mode, or you might choose to display a renewal banner.

The class includes the `getLicense()` method, which lets you check the validity of the license for the current plugin. A license error indicates an invalid or expired license. You can inspect the license status in the following form:

``` java
if (licenseManager.getLicense().isDefined())
{
   PluginLicense license = licenseManager.getLicense().get();
   if (license.getError().isDefined())
   {
        // handle license error scenario
        // (e.g., expiration or user mismatch) 
   }
   else
   {
        // handle valid license scenario
   }
} 
else
{
        // handle unlicensed scenario
}
```

We recommend that you check for license validity at each of the plugin's entry points. Upon detecting an invalid license you can either hide all UI elements, make your plugin read-only, disable your plugin, or any other behavior your define.

 

 

  [Valid License]: #valid-license
  [Conditions that Invalidate a Plugin License]: #conditions-that-invalidate-a-plugin-license
  [Handling a License Validation Error]: #handling-a-license-validation-error

