---
title: Screenshot 11305313
aliases:
    - /market/-screenshot-11305313.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11305313
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11305313
confluence_id: 11305313
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Screenshot

Displayed at 560px x 274px in carousel, no maximum size for lightbox. PNG/JPG



