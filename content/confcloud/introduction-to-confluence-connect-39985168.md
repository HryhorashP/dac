---
title: Introduction to Confluence Connect 39985168
aliases:
    - /confcloud/introduction-to-confluence-connect-39985168.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985168
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985168
confluence_id: 39985168
platform:
product:
category:
subcategory:
---
# Confluence Connect : Introduction to Confluence Connect

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>A step-by-step introduction to building a basic Confluence add-on and macro.</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
2 - BEGINNER
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>45 minutes</td>
</tr>
<tr class="even">
<td>Example</td>
<td>N/A</td>
</tr>
</tbody>
</table>

These lessons will get you started writing your very first Confluence Connect add-on step-by-step.

Want to dive in quickly and build a macro in 10 minutes? Check out the [Quick start to Confluence Connect].

# Prerequisites

Ensure you have installed all the tools you need for Confluence Connect add-on development, and running Confluence by going through the [Development setup].

# Lessons

Get started quickly by building a Connect add-on which creates a simple general page, and background-color macro.

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th>Title</th>
<th>Description</th>
<th>Estimated Time</th>
<th>Example</th>
<th>Level</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="/display/CONFCLOUD/Lesson+1+-+The+Source+Awakens">Lesson 1 - The Source Awakens</a></td>
<td>A guide to setting up a general page using ACE.</td>
<td>15 minutes</td>
<td>N/A</td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
BEGINNER
</div></td>
</tr>
<tr class="even">
<td><a href="/display/CONFCLOUD/Lesson+2+-+Rise+of+the+Macros">Lesson 2 - Rise of the Macros</a></td>
<td>A guide to adding a Confluence macro with ACE.</td>
<td>30 minutes</td>
<td><a href="https://bitbucket.org/mjensen/example-static-macro" class="uri" class="external-link">https://bitbucket.org/mjensen/example-static-macro</a></td>
<td><div class="content-wrapper" xmlns="http://www.w3.org/1999/xhtml">
BEGINNER
</div></td>
</tr>
</tbody>
</table>

# Further reading

-   [Confluence REST API]
-   [Confluence Connect patterns]
-   <a href="http://connect.atlassian.com" class="external-link">Atlassian Connect Documentation</a>

 

  [Quick start to Confluence Connect]: /confcloud/quick-start-to-confluence-connect-39987884.html
  [Development setup]: /confcloud/development-setup-39988911.html
  [Lesson 1 - The Source Awakens]: /display/CONFCLOUD/Lesson+1+-+The+Source+Awakens
  [Lesson 2 - Rise of the Macros]: /display/CONFCLOUD/Lesson+2+-+Rise+of+the+Macros
  [Confluence REST API]: /confcloud/confluence-rest-api-39985291.html
  [Confluence Connect patterns]: /confcloud/confluence-connect-patterns-39981569.html

