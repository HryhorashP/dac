---
title: Handling Page Attachments 39988795
aliases:
    - /confcloud/handling-page-attachments-39988795.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988795
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988795
confluence_id: 39988795
platform:
product:
category:
subcategory:
---
# Confluence Connect : Handling page attachments

## Graphic elements required

An icon of your content type with the following specs:

-   16x16px in size, optimised for retina display
-   Transparent background
-   Line weight of 1px for icon graphics
-   Colour \#707070 only

## How do users interact with attachments?

Users can find all the attachments on a page by clicking the <img src="https://pug.jira-dev.com/wiki/download/thumbnails/1791459341/ellipsis.png?version=1&amp;modificationDate=1463461007649&amp;api=v2" class="confluence-thumbnail confluence-external-resource" width="16" />on the top right hand of a page &gt; **Attachments**. All the files attached to a page will appear here in a table, with different tabs for each type of custom content.

## UI components

As each type of custom content is automatically sectioned into separate tabs, you can also use this to add in actions that are specific to your type of custom content.

![]

## Recommendations

-   Use the <a href="https://design.atlassian.com/product/components/tables/" class="external-link">table design guidelines</a>.

  []: /confcloud/attachments/39988795/39988794.png

