---
title: Cql Keywords Reference 39985872
aliases:
    - /confcloud/cql-keywords-reference-39985872.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985872
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985872
confluence_id: 39985872
platform:
product:
category:
subcategory:
---
# Confluence Connect : CQL Keywords Reference

A keyword in CQL is a word or phrase that:

-   joins two or more clauses together to form a complex CQL query, or
-   alters the logic of one or more clauses, or
-   alters the logic of [operators], or
-   has an explicit definition in a CQL query, or
-   performs a specific function that alters the results of a CQL query.

**List of Keywords:**

-   [AND]
-   [OR]
-   [NOT]
-   [ORDER BY]

#### AND

Used to combine multiple clauses, allowing you to refine your search.

Note: you can use [parentheses] to control the order in which clauses are executed.

###### Examples

-   Find all blogposts with the label "performance"

    ``` javascript
    label = "performance" and type = "blogpost"
    ```

-   Find all pages created by jsmith in the DEV space

    ``` javascript
    type = page and creator = jsmith and space = DEV
    ```

-   Find all content that mentions jsmith but was not created by jsmith

    ``` javascript
    mention = jsmith and creator != jsmith
    ```

[^top of keywords] | [^^top of topic]

#### OR

Used to combine multiple clauses, allowing you to expand your search.

Note: you can use [parentheses] to control the order in which clauses are executed.

(Note: also see [IN], which can be a more convenient way to search for multiple values of a field.)

###### Examples

-   Find all content in the IDEAS space or with the label idea

    ``` javascript
    space = IDEAS or label = idea
    ```

-   Find all content last modified before the start of the year or with the label needs\_review

    ``` javascript
    lastModified < startOfYear() or label = needs_review
    ```

[^top of keywords] | [^^top of topic]

#### NOT

Used to negate individual clauses or a complex CQL query (a query made up of more than one clause) using [parentheses], allowing you to refine your search.

(Note: also see [NOT EQUALS] ("!="), [DOES NOT CONTAIN] ("!~") and [NOT IN].)

###### Examples

-   Find all pages with the "cql" label that aren't in the dev space

    ``` javascript
    label = cql and not space = dev 
    ```

[^top of keywords] | [^^top of topic]

#### ORDER BY

Used to specify the fields by whose values the search results will be sorted.

By default, the field's own sorting order will be used. You can override this by specifying ascending order ("`asc`") or descending order ("`desc`").

Not all fields support Ordering. Generally, ordering is not supported where a piece of content can have multiple values for a field, for instance ordering is not supported on labels.

###### Examples

-   Find content in the DEV space ordered by creation date

    ``` javascript
    space = DEV order by created
    ```

-   Find content in the DEV space ordered by creation date with the newest first, then title

    ``` javascript
    space = DEV order by created desc, title
    ```

-   Find pages created by jsmith ordered by space, then title

    ``` javascript
    creator = jsmith order by space, title asc
    ```

[^top of keywords] | [^^top of topic]

  [operators]: #operators
  [AND]: #and
  [OR]: #or
  [NOT]: #not
  [ORDER BY]: #order-by
  [parentheses]: #parentheses
  [^top of keywords]: #^top-of-keywords
  [^^top of topic]: #^^top-of-topic
  [IN]: #in
  [NOT EQUALS]: #not-equals
  [DOES NOT CONTAIN]: #does-not-contain
  [NOT IN]: #not-in

