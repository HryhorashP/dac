---
title: JIRA Software Modules Boards 
platform: cloud
product: jswcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-software-modules-boards-39990330.html
- /jiracloud/jira-software-modules-boards-39990330.md
confluence_id: 39990330
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990330
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990330
date: "2016-09-15"
---
# Board module

This pages lists the JIRA Software modules for boards. 

**On this page:**

-   [Board area]
-   [Board configuration]

## Board area

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td>Adds a dropdown menu to a board, next to the <strong>Boards</strong> menu.</td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webSection + webPanel</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>jira.agile.board.tools</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jsw-board-menu.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;board-links&quot;,
            &quot;location&quot;: &quot;jira.agile.board.tools&quot;,
            &quot;weight&quot;: 10,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My Extension&quot;
            }
        }
    ],   
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;my-web-panel&quot;,
            &quot;url&quot;: &quot;web-panel?id={board.id}&amp;mode={board.screen}&quot;,
            &quot;location&quot;: &quot;board-links&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My Web Panel&quot;
            },
            &quot;layout&quot;: {
                &quot;width&quot;: &quot;100px&quot;,
                &quot;height&quot;: &quot;100px&quot;
            }
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Board configuration

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td>Adds a board configuration page.</td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>jira.agile.board.configuration</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jswdev-boardconfigpage.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
&quot;modules&quot;: {       
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;my-configuration-page&quot;,
            &quot;url&quot;: &quot;configuration?id={board.id}&amp;type={board.type}&quot;,
            &quot;location&quot;: &quot;jira.agile.board.configuration&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;My board configuration page&quot;
            },
            &quot;weight&quot;: 1
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Board area]: #board-area
  [Board configuration]: #board-configuration
