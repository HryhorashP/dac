---
title: Get help 
platform: cloud
product: jiracloud
category: help
subcategory: help
aliases:
- /jiracloud/support-39988134.html
- /jiracloud/support-39988134.md
confluence_id: 39988134
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988134
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988134
date: "2016-10-10"
---
# Get help

Get help from other developers, provide us with feedback on what's working and not working for you, and give back to the community. 

If you need help with anything else that is not covered by this page, get in touch with our [Developer Relations](https://developer.atlassian.com/help#contact-us) team.

Join the conversation with fellow add-on developers <a href="https://groups.google.com/forum/#!forum/atlassian-connect-dev" class="external-link">in our Google Group</a>, or ask a question to the community on <a href="https://answers.atlassian.com/questions/topics/754005/atlassian-connect" class="external-link">Atlassian answers</a>.

![Get help](../../../../illustrations/atlassian-places-24.png)

Stumped? No worries.
<a href="https://ecosystem.atlassian.net/servicedesk/customer/portal/12" class="external-link">Submit a developer support request </a>(or report a bug if you find one!)


## Atlassian Experts

Need a custom add-on, but don't want to build it yourself? Our [experts](href="https://www.atlassian.com/resources/experts) can help.
