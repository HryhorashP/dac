---
title: Extension points for project configuration
platform: cloud
product: jiracloud
category: reference
subcategory: modules 
aliases:
- /jiracloud/jira-platform-modules-project-configuration-39988366.html
- /jiracloud/jira-platform-modules-project-configuration-39988366.md
confluence_id: 39988366
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988366
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988366
date: "2016-09-15"
---
# Extension points for project configuration

This pages lists the extension points for modules on the JIRA project configuration page. 

**On this page:**

-   [Project configuration menu]
-   [Project summary area]

## Project configuration menu

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Adds sections and items to the menu of the project configuration screen</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>atl.jira.proj.config</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jira-projectconfig-menu.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example-menu-section&quot;,
            &quot;location&quot;: &quot;atl.jira.proj.config&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example section link&quot;,
            &quot;location&quot;: &quot;example-menu-section&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Project summary area

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Adds panels to the project summary area</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webPanel</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>webpanels.admin.summary.left-panels</code> and <code>webpanels.admin.summary.right-panels</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jira-projectconfig-summary.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
&quot;modules&quot;: {
    &quot;webPanels&quot;: [
        {
            &quot;key&quot;: &quot;example-panel&quot;,
            &quot;location&quot;: &quot;webpanels.admin.summary.left-panels&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example panel&quot;
            }
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Project configuration menu]: #project-configuration-menu
  [Project summary area]: #project-summary-area
