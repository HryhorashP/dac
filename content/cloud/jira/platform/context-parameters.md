---
aliases:
    - /jiracloud/context-parameters.html
    - /jiracloud/context-parameters.md
title: Context parameters
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2016-10-05"
---
{{< include path="content/cloud/connect/concepts/context-parameters.snippet.md">}}
