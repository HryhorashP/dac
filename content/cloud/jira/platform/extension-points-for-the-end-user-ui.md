---
title: "Extension points for the end-user UI" 
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-user-accessible-locations-39988374.html
- /jiracloud/jira-platform-modules-user-accessible-locations-39988374.md
confluence_id: 39988374
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988374
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988374
date: "2016-09-30"
---
#  Extension points for the end-user UI 

This pages lists the extension points for modules for locations in the JIRA UI that are accessible by non-admin users.

**On this page:**

-   [Top navigation bar location]
-   [User name drop-down location]
-   [User profile page dropdown location]
-   [Hover profile links location]
-   [Dialog box hint location]

## Top navigation bar location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Defines web items in JIRA's top navigation bar, which are accessible from all JIRA areas (except JIRA's administration area/mode).</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webItem </code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>system.top.navigation.bar</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jdev-header.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...

&quot;modules&quot;: {
    &quot;webItems&quot;: [
        {
            &quot;location&quot;: &quot;system.top.navigation.bar&quot;,
            &quot;key&quot;: &quot;example-section-link&quot;,
            &quot;name&quot;: { &quot;value&quot;: &quot;Example add-on link&quot; },
            &quot;url&quot;: &quot;http://www.example.com&quot;,
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## User name drop-down location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Defines web sections and items in JIRA's user name and help drop-down menus, which are accessible from all JIRA screens.</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><ul>
    <li><code>system.user.options</code> &mdash; profile and help menus in top right of header bar</li>
    <li><code>/personal, /set_my_jira_home, /system, /jira-help</code> &mdash; sections within the profile and help menus (see screenshot below)</li>
</ul></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jdev-userprofile.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "system.user.options",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "system.user.options/personal",
            "name": {
                "value": "Example add-on link"
            },
            "url": "/example-section-link
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## User profile page dropdown location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Defines web items of the more (<img src="../images/ellipsis.png"/>) dropdown menu on a JIRA user's user profile page.<br />
This location has only one section ( <code>operations</code> ) to which custom web items can be added.</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webItem</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>system.user.profile.links/operations </code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jdev-userprofilepagedropdown-location.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
&quot;modules&quot;: {
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example-section-link&quot;,
            &quot;location&quot;: &quot;system.user.profile.links/operations&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            },
            &quot;url&quot;: &quot;/example-link&quot;
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Hover profile links location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Defines web items in JIRA's hover profile feature, which is accessible when a user hovers their mouse pointer over a JIRA user's name throughout JIRA's user interface</p></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>webSection + webItem</code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>system.user.hover.links</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jdev-userprofilehover-location.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example-menu-section&quot;,
            &quot;location&quot;: &quot;system.user.hover.links&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example-section-link&quot;,
            &quot;location&quot;: &quot;example-menu-section&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Dialog box hint location

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td><p>Defines web items that allow you to add hints on JIRA's dialog boxes. You can add hints to most JIRA dialog boxes.</p>
<p>To add your own web item to JIRA's dialog box hints location for a specific dialog box, your web item must include a <code>section</code> attribute with the value<code>&quot;jira.hints/LOCATION_CONTEXT&quot;.</code>The <code style="line-height: 1.42857;">LOCATION_CONTEXT</code> is a predefined 'context' in JIRA that determines on which dialog box your hints will appear:</p>
<ul>
<li><code>TRANSITION</code> -- Hints on a 'transition issue' dialog box.</li>
<li><code>ASSIGN</code> -- Hints on the 'Assign' dialog box.</li>
<li><code>LABELS</code> -- Hints on the 'Labels' dialog box.</li>
<li><code>COMMENT</code> -- Hints on 'Comment' dialog boxes.</li>
<li><code>CLONE</code> -- Hints on 'Clone Issue' dialog boxes.</li>
<li><code>DELETE_FILTER</code> -- Hints on 'Deletre Filter' dialog boxes.</li>
<li><code>ATTACH</code> -- Hints on 'Attach Files' dialog boxes, not the 'Attach Screenshot' dialog.</li>
<li><code>DELETE_ISSUE</code> -- Hints on 'Delete issue' dialog boxes.</li>
<li><code>LINK</code> -- Hints on 'Link issue' dialog boxes.</li>
<li><code>LOG_WORK</code> -- Hints on 'Log work' dialog boxes.</li>
</ul></td>
</tr>
<tr class="even">
<td><strong>Module type</strong></td>
<td><code>  webSection + webItem </code></td>
</tr>
<tr class="odd">
<td><strong>Location key</strong></td>
<td><code>jira.hints</code></td>
</tr>
<tr class="even">
<td><strong>Screenshot</strong></td>
<td><p><img src="../images/jdev-dialogboxhint-location.png"/></p></td>
</tr>
<tr class="odd">
<td><strong>Sample JSON</strong></td>
<td><div class="content-wrapper">
<div class="code pdl panel" style="border-width: 1px;">
<div class="pdl codeContent panelContent">
{{< highlight javascript >}}...
&quot;modules&quot;: {
    &quot;webSections&quot;: [
        {
            &quot;key&quot;: &quot;example-menu-section&quot;,
            &quot;location&quot;: &quot;jira.hints/ASSIGN&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on name&quot;
            }
        }
    ],
    &quot;webItems&quot;: [
        {
            &quot;key&quot;: &quot;example-section-link&quot;,
            &quot;location&quot;: &quot;example-menu-section&quot;,
            &quot;name&quot;: {
                &quot;value&quot;: &quot;Example add-on link&quot;
            }
        }
    ]
}
...{{< /highlight >}}
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

  [Top navigation bar location]: #top-navigation-bar-location
  [User name drop-down location]: #user-name-drop-down-location
  [User profile page dropdown location]: #user-profile-page-dropdown-location
  [Hover profile links location]: #hover-profile-links-location
  [Dialog box hint location]: #dialog-box-hint-location
