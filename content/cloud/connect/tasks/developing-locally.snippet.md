# Developing locally

In this guide you will install your locally running add-on into your Atlassian Cloud development environment; this is the
development environment that you created during the [Development setup][1] guide.

In order to get an add-on that is running on your personal computer (locally) installed in an Atlassian Cloud instance, you
need to make it available on the internet, served over an HTTPS connection.

If you are using [atlassian-connect-express][5] (ACE), auto registration happens automatically. Read more about it in the [README][4].

To achieve this, there are many tools you could use, but we recommend [ngrok](https://ngrok.com/). ngrok is a simple and
free command line tool to tunnel your local development environment to the internet. To use ngrok:

1. Download and install ngrok.
    * If you have [npm][3] installed, you can use: `npm install -g ngrok`
    * Otherwise you can download it from the [ngrok website](https://ngrok.com/download)
1. Find out what port your add-on is running on.

   This can generally be done by running the add-on and looking for the port in the logs or
   by checking the project README for details.
   If you need a project, check out [Connect Basics][2].

1. From the command line, run `ngrok http $PORT` replacing `$PORT` with the port used by your add-on.
1. Get the HTTPS URL from the command line, as shown by the following image:
   <img width="60%" src="/cloud/connect/images/ngrok-example-shadow-arrow.jpg" />
1. Change the add-on's JSON descriptor so that the `baseUrl` is set to the ngrok URL from the previous step.
1. Check that your descriptor now shows the correct `baseUrl`.

   You can do this by navigating to the ngrok HTTPS URL with the path to your descriptor appended. Using the example above
   your descriptor URL might look something like: `https://02b76172.ngrok.io/atlassian-connect.json`. When your descriptor
   loads, ensure that the `baseUrl` is the publicly accessible ngrok URL.

   <div class="aui-message">
   Ensure that your add-on is active / running when you do this, otherwise you will only get "Failed to complete tunnel connection".
   </div>

Now that your add-on is accessible on the internet, over HTTPS, you can now install it into your development
environment in the same way that you installed the example add-on in the [Development setup][1] guide.

 [1]: ../guides/development-setup.html
 [2]: ../tutorials/connect-basics.html
 [3]: https://www.npmjs.com/
 [4]: https://bitbucket.org/atlassian/atlassian-connect-express#markdown-header-automatic-registration
 [5]: https://bitbucket.org/atlassian/atlassian-connect-express/overview
