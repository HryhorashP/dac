### Install Node.js

You can skip this step if you have Node.js.

We'll be using a Node.js powered static webserver in this tutorial.
If you don't yet have Node.js installed, you can find the
<a href="https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager" target="_blank">installation instructions for your system here</a>.
