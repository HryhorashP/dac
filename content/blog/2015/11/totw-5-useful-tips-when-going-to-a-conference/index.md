---
title: "Tip of the Week - 5 tips for when you go to a Conference."
date: "2015-11-02T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","conference"]
lede: "Going to conferences can both be a reward for a job well done and a whole bunch of training sessions
  back to back. In this Tip of the Week we'll be providing you with 5 tips to
  the most out of your conference."
---
This week it's Atlassian Summit in San Francisco. Because of this I want to
 share my 5 tips to get the most out of any conference you attend.

Plan
----
Before the conference you should try to take some time to create a plan and
 take the following into account:
 - Does the conference provide videos of the sessions?
   If yes, this can help you to choose the sessions you really want to see live,
   all others you can simply view later from the comfort of your home.
 - Which extra events do you want to take part in? (Networking events, dinners, concerts,...)
   Make sure to put them in your agenda and don't forget to calculate time for
   getting to the venues. Nothing is less fun then finally arriving when the event is almost finished.
 - Who do you want to meet up with? Make sure to set up those meetings beforehand!
 - Plan for at least one decent meal a day to give your body some needed energy and
   your mind some rest. You can always invite somebody to join you if you don't feel like
   eating alone.

Don't forget the hallway track
------------------------------
A lot of conferences have different tracks (Summit has 7 tracks this year),
but the most important one is the "Hallway Track". I've learned more by having hallway
conversations and meeting people by accident than by following sessions.
It's also a great way to network and meet up with old friends.

You can always watch a session video afterwards but you cannot replace those missed
hallway conversations.

Visit the sponsors
------------------
Thanks to sponsors, conferences can keep ticket prices low and do the crazy things
we all love - like movies, concerts, and so on. So take some time to talk to them
 and you might get some cool swag and some interesting information in return.

And of course you can always go and thank them for building the tools you use and love.

Take care of yourself
---------------------
Conferences can be exhausting: running from one session to another, grabbing a quick snack
in between, going to late night events and sleeping too little.

Be sure to still take care of yourself, both physically and mentally. Take a regular break,
 take time to have at least one proper meal everyday.
 Make sure you get enough sleep before or during the conference.

Enjoy the evenings
------------------
Conferences don't stop after the final session of the day. There will be many opportunities to network, dinners and
 other events. Be sure to go to as much of these as possible and use to meet
 new people. These are the perfect networking events and can help you relax after a long conference day.

***

Do you have any tips or tricks for going to conferences?
<a href="https://developer.atlassian.com/blog/authors/rwhitbeck/"/>Ralph Whitbeck </a>
already shared his tips earlier this year: <a href=" https://developer.atlassian.com/blog/2015/04/maximize-your-next-conference-with-these-tips/">
Maximize your next conference with these tips.</a>

Make sure to share them in the comments, ping me on twitter: <a href="https://twitter.com/pvdevoor">@pvdevoor</a>.
Or simply tell them to me personally when you see me at a conference.
