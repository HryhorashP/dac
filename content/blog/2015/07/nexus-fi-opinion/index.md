---
title: "Analysis: A new Nexus 5 edition is all about the Fi"
date: "2015-07-08T06:00:00+07:00"
author: "ssmith"
categories: ["opinion", "google", "nexus", "android", "analysis"]
---

<style>
  .right-image {
    float: right;
    margin: 1em;
  }
</style>

The
[usual](http://www.techradar.com/news/phone-and-communications/mobile-phones/it-looks-like-lg-is-building-the-nexus-5-2015--1298372)
[rumour](http://tech.firstpost.com/news-analysis/google-nexus-5-2015-lgs-next-rumoured-nexus-phone-272934.html)
[mills](http://bgr.com/2015/06/29/lg-nexus-5-2015-release-date/) are
all fired up over the possibility of new Nexus 5 & 6 editions
appearing this year, mostly over who would be making them. Most of the
justifications given for a new Nexus 5 in particular seems to focus on
either the new Android M hardware features such as fingerprint
recognition and ARMv8. However, there is one hardware feature that I
haven't seen discussed anywhere, and it's the one reason why I feel
Google absolutely _must_ release a new Nexus 5 model this year. That
reason is simply [Project Fi](https://fi.google.com/about/).

## Project Fi ##

<img class="right-image" style="width: 300px;" alt="Project Fi" src="fi-logo.png"/>

Google has been trying to disrupt the US mobile industry since 2010,
when it made the original
[Nexus One](https://en.wikipedia.org/wiki/Nexus_One) available
unlocked on its own store. This went against US model of releasing
phones only with a carrier's plan; this practice
[famously frustrated Apple fans](http://appleinsider.com/articles/11/01/12/emdaily_show_em_skewers_att_over_verizon_iphone_news),
who for years could only get the original iPhone on AT&T. Google's
previous attempts at disruptions seems to have had little effect
however.

Project Fi is still invitation-only and there are few details on its
implementation, but appears to be Google's latest attempt to maneuver
around the mobile operators. It does this by treating them as a
network of networks, supplemented with WiFi hotspots. This removes the
user from the tyranny of having to choose a single carrier by allowing
the phone to choose the best option at any given point. Google acts as
the broker between the networks (whether that will put users under the
tyranny of Google remains to be seen).

## Fi needs hardware ##

The problem at the moment however is that
[Project Fi is only accessible to one phone model; the Nexus 6](https://fi.google.com/about/faq/#supported-devices-1). This
is because Fi requires dedicated hardware, and the Nexus 6 is only phone
that has it.

It's not clear what this hardware actually does, but it's almost
certainly related to managing the interaction between the networks (it
may also handle encryption, although the Qualcomm Snapdragon 808 has a
native encryption module). The problem is that automatic hand-off
between networks of different types (as opposed to hand-off between
different cellular towers on the same network) is hard to do. The
reason is that it must do so seamlessly, possibly while the user is on
an active call. This can be tricky to do even when the network is
under a common control, which is why seamless WiFi roaming requires a
custom hardware, usually in the form of
[Zero Handoff](https://community.ubnt.com/t5/UniFi-Frequently-Asked-Questions/UniFi-What-is-Zero-Handoff/ta-p/412719)
base-stations. On disparate networks that are unaware of each-other
it's going to be up to the client to somehow mediate the flow of
packets to provide a smooth experience.

## Fi needs Nexus. A popular one. ##

So Fi requires custom hardware, and only one phone has it. And
unfortunately it's not a particularly popular phone; the
[Nexus 6 sales were disappointing](http://www.theinquirer.net/inquirer/news/2405892/google-admits-nexus-6-and-nexus-9-sales-have-been-sluggish),
especially compared to the
[wildly popular Nexus 5](http://www.theverge.com/2014/1/30/5362316/not-just-for-phone-nerds-google-calls-nexus-5-a-sales-winner).
Google could rely on 3rd-party manufacturers to include support, but
they're not going do so until Fi is popular, and Fi will remain niche
until there's widespread support: catch-22. So it's up to the Nexus
line to drive the adoption and encourage the 3rd-parties to follow
along. But to a degree the Nexus line has always been about providing
flagship features first, so this is nothing new.

## Put a pin in it ##

So to put my reputation where my mouth is, here's some
predictions. Come back in January to see how I did:

1. We can expect at least one new Nexus phone this year, probably
   several.
1. One of these will be an updated version of the Nexus 5.
1. To really put it on the line, I expect the new Nexus 5 hardware to
   be Snapdragon 808, 32GB and 64GB models, fingerprint scanner, and
   Fi support.
1. There may also be dual-SIM support as that's also an Android M
   feature, although I'm less confident in that.
1. The price-point of the new Nexus 5 will be very reasonable,
   probably similar to the 2013 version. The other phone(s) may be
   priced closer to the market though.

Let's see how this goes, shall we :) I'm going out on a limb here a
bit, but I think my reasoning is sound. But I'd be interested in
hearing other views, so feel free to jump in on the comments below or
ping me on Twitter [@tarkasteve](https://twitter.com/tarkasteve)
