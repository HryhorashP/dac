import { combineReducers } from 'redux'
import { REQUEST_AID_PROFILE,
         RECEIVE_AID_PROFILE,
         REQUEST_LOGIN_AID_USER,
         RECEIVE_LOGIN_AID_USER } from '../actions'

const initialState = 0;

const counter = (state = initialState, action) => {
  switch (action.type) {
    case 'INCREASE':
      return state + 1
    case 'DECREASE':
      return state - 1
    default:
      return state
  }
}

function profile(state = {
    isFetching: false,
    authenticated: false,
    didInvalidate: false,
    profile: {},
    openid_auth: {}
  }, action) {
  switch (action.type) {
    case REQUEST_AID_PROFILE:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case RECEIVE_AID_PROFILE:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        authenticated: action.authenticated,
        profile: action.profile,
        lastUpdated: action.receivedAt
      })
    case REQUEST_LOGIN_AID_USER:
      return Object.assign({}, state, {
        didInvalidate: false
      })
    case RECEIVE_LOGIN_AID_USER:
      return Object.assign({}, state, {
        didInvalidate: false,
        openid_auth: action.openid_auth,
        lastUpdated: action.receivedAt
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  counter,
  profile
})

export default rootReducer
